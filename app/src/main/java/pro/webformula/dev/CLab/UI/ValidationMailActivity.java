package pro.webformula.dev.CLab.UI;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pro.webformula.dev.CLab.Controllers.UserProcessedController;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 17.03.2015.
 */
public class ValidationMailActivity extends ActionBarActivity {

    private UserProcessedController controller;
    EditText editTextCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_validation_mail);
        controller = UserProcessedController.getController(getApplicationContext());

        TextView textViewMail = (TextView)findViewById(R.id.customer_mail);
        editTextCode = (EditText)findViewById(R.id.customer_code);

        editTextCode.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextCode.setTextColor(getResources().getColor(android.R.color.black));
                return false;
            }
        });
        getActionBar().setIcon(R.drawable.trp);

        textViewMail.setText(controller.getMail());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menu.add(0, R.menu.menu_main, 1, R.string.yes)
                .setIcon(R.drawable.close)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        controller.setMail("");
                        Intent intent = new Intent(getApplicationContext(),UserProcessedActivity.class);
                        intent.putExtra(MainActivity.EXTRA_NEW_USER,true);
                        startActivity(intent);
                        finish();
                        return false;
                    }
                })
                .setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add(0, R.menu.menu_main, 1, R.string.yes)
                .setIcon(R.drawable.ok)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (validateCode()) {
                            ActionTask task = new ActionTask();
                            task.execute();
                            try {
                                resultOfAction(task.get());
                            } catch (Exception ex) {
                                Log.e("Task", ex.getLocalizedMessage());
                            }
                        }
                        return false;
                    }
                })
                .setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);

                    return true;
                }

        @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public boolean validateCode(){
        if(editTextCode.getText().toString().length()!=4){
            return false;
        }
        return true;
    }

    public void resultOfAction(int code){
        switch (code){
            case 0:
                controller.setMail("");
                Intent intent = new Intent(getApplicationContext(),ResultActivity.class);
                intent.putExtra("status",1);
                startActivity(intent);
                finish();
                break;
            case 1203:
                setErrorField();
                break;
            default:
                Toast.makeText(getApplicationContext(),"Fail connection",Toast.LENGTH_LONG).show();
                break;
        }
    }

    public void setErrorField(){
        editTextCode.setTextColor(getResources().getColor(android.R.color.holo_red_light));
    }

    private class ActionTask extends AsyncTask<Void,Void,Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            return controller.validationMailOnServer(editTextCode.getText().toString());
        }

    }
}
