package pro.webformula.dev.CLab.UI;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pro.webformula.dev.CLab.Controllers.Controllers;
import pro.webformula.dev.CLab.Controllers.FilialAgencyController;
import pro.webformula.dev.CLab.Controllers.MedicalTestController;
import pro.webformula.dev.CLab.Controllers.OrderController;
import pro.webformula.dev.CLab.POJO.FilialAgency;
import pro.webformula.dev.CLab.POJO.MedicalTest;
import pro.webformula.dev.CLab.POJO.Order;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 27.02.2015.
 */
public class FragmentSearchResult extends Fragment {

    private TextView searchTextView;
    private ListView listViewResultSearch;
    private String additionalText;
    private Controllers controller;
    private ArrayList<Object> resultList;
    private ArrayList<Integer> testId;
    private Intent intent;

    private SearchAdapter adapter;

    int type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getInt(SearchActivity.SEARCH_TYPE,0);
        intent = getActivity().getIntent();
        switch (type){
            case 0:
                controller = OrderController.getController(getActivity().getApplicationContext());
                break;
            case 1:
                controller = FilialAgencyController.getController(getActivity().getApplicationContext());
                break;
            case 2:
                controller = MedicalTestController.getController(getActivity().getApplicationContext());
                break;
        }
        resultList = new ArrayList<Object>();
        testId = new ArrayList<Integer>();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_result_search, null);
        searchTextView = (TextView)view.findViewById(R.id.searched_text);
        searchTextView.setText(additionalText);

        listViewResultSearch = (ListView)view.findViewById(R.id.list_search_result);
        adapter = new SearchAdapter(getActivity().getApplicationContext(),resultList);
        listViewResultSearch.setAdapter(adapter);
        listViewResultSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (type){
                    case 0:
                        for(Order order: OrderController.getController(getActivity().getApplicationContext()).getOrderList()){
                            if(order.equals((Order)adapter.getItem(position))){
                                Intent intent = new Intent(getActivity().getApplicationContext(),OrderActivity.class);
                                intent.putExtra(OrdersFragment.ORDER_EXTRA_UUID,order.getOrderUUID());
                                startActivityForResult(intent,1);
                                break;
                            }
                        }
                        break;
                    case 1:
                        int idFilial=0;
                        for(FilialAgency filialAgency: FilialAgencyController.getController(getActivity().getApplicationContext()).getFilialAgencies()){
                            if(filialAgency.equals((FilialAgency)adapter.getItem(position))){
                                Intent intent = new Intent(getActivity().getApplicationContext(),FilialAgencyActivity.class);
                                intent.putExtra(MapsFragment.EXTRA_FILIAL,idFilial);
                                startActivityForResult(intent,1);
                                break;
                            }
                            idFilial++;
                        }
                        break;
                    case 2:
                        String desc = ((MedicalTest)adapter.getItem(position)).getDesk();
                        if(!desc.equals(""))
                            showWebViewDialog(desc);
                        break;
                }

            }
        });
        searchResult();
        return view;
    }

    public void setSearchText(String text){
        additionalText = text;
        if(searchTextView != null){
            resultList.clear();
            searchTextView.setText(text);
            searchResult();
        }
    }

    public void searchResult(){
        resultList.addAll(controller.getSearchResult(additionalText));
        adapter.notifyDataSetChanged();
    }

    private class SearchAdapter extends ArrayAdapter<Object>{

        ArrayList<Object> values;
        Context context;

        public SearchAdapter(Context context, ArrayList<Object> values){
            super(context, R.layout.item_filial, values);
            this.values = values;
            this.context = context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            switch (type){
                case 0:
                    return getViewOrder(position,convertView,parent);
                case 1:
                    return getViewFilial(position,convertView,parent);
                case 2:
                    return getViewMedicalTest(position,convertView,parent);
            }
           return convertView;
        }
        public View getViewFilial(final int position, View convertView, ViewGroup parent){
            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_filial, parent, false);
            }

            TextView textViewTitle = (TextView)convertView.findViewById(R.id.filial_name);
            TextView textViewTime = (TextView)convertView.findViewById(R.id.filial_adress);
            textViewTitle.setText(((FilialAgency)values.get(position)).getAddress());
            textViewTime.setText(((FilialAgency)values.get(position)).getPhone());

            return convertView;
        }

        public View getViewOrder(final int position, View convertView, ViewGroup parent){
            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_orders, parent, false);
            }

            TextView textViewCustomer = (TextView)convertView.findViewById(R.id.orders_customer);
            TextView textViewStatus = (TextView)convertView.findViewById(R.id.orders_status);
            TextView textViewTest = (TextView)convertView.findViewById(R.id.orders_test);
            TextView textViewTime = (TextView)convertView.findViewById(R.id.orders_date);
            TextView textViewCost = (TextView)convertView.findViewById(R.id.orders_cost);
            TextView textViewTestCompleted = (TextView)convertView.findViewById(R.id.orders_test_completed);


            int statusId = getResources().getIdentifier(((Order)values.get(position)).getStatus().toString().toLowerCase(),
                    "drawable",context.getPackageName());
            int statusIdColor = getResources().getIdentifier(((Order)values.get(position)).getStatus().toString(),
                    "color",context.getPackageName());
            String passedTest[] = ((Order)values.get(position)).getTestTitle();

            textViewCustomer.setText(((Order)values.get(position)).getCustomer().getCustomerTitle());
            textViewTestCompleted.setText((passedTest[0]));
            textViewTest.setText(passedTest[1]);
            textViewStatus.setText(((Order)values.get(position)).getStatusString());
            textViewStatus.setBackgroundResource(statusId);
            textViewStatus.setTextColor(getResources().getColor(statusIdColor));
            textViewTime.setText(((Order)values.get(position)).getDateCreate());
            textViewCost.setText(((Order)values.get(position)).getCostTitle());

            return convertView;
        }

        public View getViewMedicalTest(final int position, View convertView, ViewGroup parent){
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_medical_test, parent, false);
            }

            MedicalTest medicalTest = (MedicalTest)values.get(position);

            ImageView imageViewIcon = (ImageView)convertView.findViewById(R.id.item_test_img);
            TextView textViewTitle = (TextView)convertView.findViewById(R.id.item_test_title);
            TextView textViewDay = (TextView)convertView.findViewById(R.id.item_test_day);
            TextView textViewCost = (TextView)convertView.findViewById(R.id.item_test_cost);
            TextView textViewId = (TextView)convertView.findViewById(R.id.item_test_id);
            ImageView imageViewSelect = (ImageView)convertView.findViewById(R.id.item_test_selected);

            textViewTitle.setText(medicalTest.getName());
            textViewDay.setText(medicalTest.getTime(1));
            textViewCost.setText(medicalTest.getPrice(true));
            textViewId.setText(medicalTest.getNumber());

            int idIcon = getResources().getIdentifier(medicalTest.getImg(),
                    "drawable", context.getPackageName());
            imageViewIcon.setImageResource(idIcon);
            if(testId.contains(medicalTest.getId())){
                imageViewSelect.setImageResource(R.drawable.active);
            }else{
                imageViewSelect.setImageResource(R.drawable.noactive);
            }

            imageViewSelect.setOnClickListener(new View.OnClickListener() {
                ImageView imageViewSelect;

                @Override
                public void onClick(View v) {
                    if (testId.contains(((MedicalTest) adapter.getItem(position)).getId())) {
                        imageViewSelect = (ImageView) v.findViewById(R.id.item_test_selected);
                        imageViewSelect.setImageResource(R.drawable.noactive);
                        onUnSelectItem(((MedicalTest) adapter.getItem(position)).getId());
                    } else {
                        imageViewSelect = (ImageView) v.findViewById(R.id.item_test_selected);
                        imageViewSelect.setImageResource(R.drawable.active);
                        onSelectItem(((MedicalTest) adapter.getItem(position)).getId());
                    }
                }
            });

            return convertView;
        }

        public void onSelectItem(int i){
            testId.add(new Integer(i));
        }

        public void onUnSelectItem(int i){
            testId.remove(new Integer(i));
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        int id = data.getIntExtra("id",0);
        Intent intent = getActivity().getIntent();
        intent.putExtra("idResult",id);
        getActivity().setResult(getActivity().RESULT_OK,intent);
        getActivity().finish();
    }



    public void onSelectedTestSend(){

        if(testId!=null&&testId.size()>0){
            int[] res = new int[testId.size()];
            int iteration = 0;
            for(Integer id : testId){
                res[iteration] = id;
                iteration++;
                intent.putExtra("idTests",res);
            }
        }
        getActivity().setResult(getActivity().RESULT_OK,intent);
        getActivity().finish();

    }
    public void showWebViewDialog(String desc){
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Анализ");

        if("".equals(desc) || desc == null){
            alert.setMessage("Раздел находится в наполнении");
        } else {
            WebView wv = new WebView(getActivity());
//          wv.loadUrl(desc);
            wv.loadData(desc, "text/html; charset=UTF-8", null);
            wv.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });

            alert.setView(wv);
        }
        alert.setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

}
