package pro.webformula.dev.CLab.POJO;

import java.util.UUID;

/**
 * Created by dev on 13.02.2015.
 */
public class MedicalTest {

    private int id;
    private String uuid;
    private String name;
    private double price;
    private int time;
    private String img;
    private String type;
    private String number;
    private String desk;
    private String searchString;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUUID() {
        return uuid;
    }

    public void setUUID(String id) {
        this.uuid = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        searchString +=name;
    }

    public double getPrice() {
        return price;
    }

    public String getPrice(boolean i) {
        return ""+ getPrice();
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getTime() {
        return time;
    }
    public String getTime(int i) {
        return ""+ getTime();
    }
    public void setTime(int time) {
        this.time = time;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        searchString +=" "+type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {

        this.number = number;
        searchString+=" "+number;
    }

    public String getSearchString(){
        return searchString;
    }


    public String getDesk() {
        return desk;
    }

    public void setDesk(String desk) {
        this.desk = desk;
    }

    public String getName(int size){
        if(this.name.length()<=size)
            return this.name;
        String res = this.name.substring(0,size-3);
        return res+"...";
    }
}
