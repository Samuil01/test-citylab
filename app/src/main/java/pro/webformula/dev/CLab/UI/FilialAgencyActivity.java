package pro.webformula.dev.CLab.UI;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import pro.webformula.dev.CLab.Controllers.FilialAgencyController;
import pro.webformula.dev.CLab.POJO.FilialAgency;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 14.03.2015.
 */
public class FilialAgencyActivity extends Activity{

    private MapView mapView;
    private GoogleMap map;
    private FilialAgency filialAgency;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filial_agency);
        getActionBar().hide();

        final int id = getIntent().getIntExtra(MapsFragment.EXTRA_FILIAL,0);

        filialAgency = FilialAgencyController.getController(getApplicationContext()).getFilialAgencies().get(id);


        TextView title = (TextView)findViewById(R.id.filial_title);
        TextView address = (TextView)findViewById(R.id.filial_agency_address);
        final TextView phone = (TextView)findViewById(R.id.filial_agency_phone);
        final TextView site = (TextView)findViewById(R.id.filial_agency_site);
        TextView workTime = (TextView)findViewById(R.id.filial_agency_work_time);
        ImageView back = (ImageView)findViewById(R.id.item_map_back);

        title.setText(filialAgency.getAddress());
        address.setText(filialAgency.getAddress());
        phone.setText(filialAgency.getPhone());
        site.setText(filialAgency.getSite());
        workTime.setText(filialAgency.getTime());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber= phone.getText().toString().replaceAll("-", "");
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNumber));
                startActivity(callIntent);
            }
        });

        site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse(site.getText().toString()));
                startActivity(browserIntent);
            }
        });

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            Log.e("Address Map", "Could not initialize google play", e);
        }

        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) )
        {
            case ConnectionResult.SUCCESS:
                mapView = (MapView)findViewById(R.id.map_filial);
                mapView.onCreate(savedInstanceState);
                if(mapView!=null)
                {
                    map = mapView.getMap();
                    map.getUiSettings().setMyLocationButtonEnabled(false);
                    map.setMyLocationEnabled(true);
                    map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Intent intent = getIntent();
                            intent.putExtra("idResult",id);
                            setResult(RESULT_OK,intent);
                            finish();
                        }
                    });
                    setInfo();
                }
                break;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(this, "SERVICE MISSING", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(this, "UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                break;
            default: Toast.makeText(this, GooglePlayServicesUtil.isGooglePlayServicesAvailable(this), Toast.LENGTH_SHORT).show();
        }

    }

    public void setInfo(){
        LatLng latLng = new LatLng(filialAgency.getCoordinateX(),filialAgency.getCoordinateY());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
        map.animateCamera(cameraUpdate);
        MarkerOptions marker = new MarkerOptions();
        marker.position(latLng);
        marker.title("Перейти на карту");
        map.addMarker(marker);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
