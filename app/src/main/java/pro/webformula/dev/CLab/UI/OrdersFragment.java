package pro.webformula.dev.CLab.UI;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import pro.webformula.dev.CLab.POJO.Order;
import pro.webformula.dev.CLab.Controllers.OrderController;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 11.02.2015.
 */
public class OrdersFragment extends Fragment {

    public static final String ORDER_EXTRA_UUID = "uuid";

    private OrderController controller;
    private boolean isChanged;
    private OrdersAdapter adapter;

    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, null);

        ListView listView = (ListView)view.findViewById(R.id.list_orders);
        controller = OrderController.getController(getActivity().getApplicationContext());

        ArrayList<Order> orderList = controller.getOrderList();

        adapter = new OrdersAdapter(getActivity().getApplicationContext(),orderList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity().getApplicationContext(),OrderActivity.class);
                intent.putExtra(ORDER_EXTRA_UUID,adapter.getItem(position).getOrderUUID());
                getActivity().startActivity(intent);
            }
        });
        adapter.notifyDataSetChanged();
        return view;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        menu.add(0, R.menu.menu_main, 0, R.string.news)
                .setIcon(R.drawable.plus)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = new Intent(getActivity().getApplicationContext(),OrderActivity.class);
                        getActivity().startActivity(intent);
                        return true;
                    }
                })
                .setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add(0, R.menu.menu_main, 1, R.string.news)
                .setIcon(R.drawable.search)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = new Intent(getActivity().getApplicationContext(),SearchActivity.class);
                        intent.putExtra(SearchActivity.SEARCH_TYPE,0);
                        startActivity(intent);
                        return false;
                    }
                })
                .setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPause() {
        super.onPause();
        new UpdateTask().execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isChanged || controller.isAdd()){
            adapter.clear();
            adapter.addAll(controller.getOrderList());
            adapter.notifyDataSetChanged();

            Log.e("Order","updateList");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    private class  OrdersAdapter extends ArrayAdapter<Order> {

        ArrayList<Order> values;
        Context context;

        public OrdersAdapter(Context context, ArrayList<Order> values){
            super(context, R.layout.item_news, values);
            this.values = values;
            this.context = context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_orders, parent, false);
            }

            TextView textViewCustomer = (TextView)convertView.findViewById(R.id.orders_customer);
            TextView textViewStatus = (TextView)convertView.findViewById(R.id.orders_status);
            TextView textViewTest = (TextView)convertView.findViewById(R.id.orders_test);
            TextView textViewTime = (TextView)convertView.findViewById(R.id.orders_date);
            TextView textViewCost = (TextView)convertView.findViewById(R.id.orders_cost);
            TextView textViewTestCompleted = (TextView)convertView.findViewById(R.id.orders_test_completed);


            int statusIdBorder = getResources().getIdentifier(values.get(position).getStatus().toString().toLowerCase(),
                    "drawable",context.getPackageName());
            int statusIdColor = getResources().getIdentifier(values.get(position).getStatus().toString(),
                    "color",context.getPackageName());
            String passedTest[] = values.get(position).getTestTitle();

            textViewCustomer.setText(values.get(position).getCustomer().getCustomerTitle());
            textViewTestCompleted.setText((passedTest[0]));
            textViewTest.setText(passedTest[1]);
            textViewStatus.setText(values.get(position).getStatusString());
            textViewStatus.setBackgroundResource(statusIdBorder);
            textViewStatus.setTextColor(getResources().getColor(statusIdColor));
            textViewTime.setText(values.get(position).getDateCreate());
            textViewCost.setText(values.get(position).getCostTitle());


            return convertView;
        }

    }

    public void refresh(){
        adapter.clear();
        adapter.addAll(controller.getOrderList());
        adapter.notifyDataSetChanged();
    }

    public class UpdateTask extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... params) {
            controller.sendOrdersFromDB();
            isChanged = controller.getServerOrder();
            return null;
        }
    }
}