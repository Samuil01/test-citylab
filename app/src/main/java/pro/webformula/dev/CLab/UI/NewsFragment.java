package pro.webformula.dev.CLab.UI;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pro.webformula.dev.CLab.POJO.News;
import pro.webformula.dev.CLab.Controllers.NewsController;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 11.02.2015.
 */
public class NewsFragment extends Fragment {

    private NewsController controller;
    private NewsAdapter mAdapter;
    private ArrayList<News> newsList;
    private ListView listView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newsList = new ArrayList<News>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, null);

        File cacheDir = StorageUtils.getOwnCacheDirectory(getActivity().getApplicationContext(),"UniversalImageLoader/Cache");

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .diskCache(new UnlimitedDiscCache(cacheDir))
                .build();
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.news_loader)
                .showImageOnFail(R.drawable.news_loader)
                .showImageOnLoading(R.drawable.news_loader)
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresher);
        listView = (ListView)view.findViewById(R.id.list_news);
        controller = NewsController.getController(getActivity().getApplicationContext());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            boolean isAction = bundle.getBoolean("Action",false);

            if(isAction){
                new FirstLoadTask().execute();
            }
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initiateRefresh();
            }
        });

        mSwipeRefreshLayout.setColorScheme(R.color.color_scheme_1_1, R.color.color_scheme_1_2,
                R.color.color_scheme_1_3, R.color.color_scheme_1_4);
        mAdapter = new NewsAdapter(getActivity().getApplicationContext(),newsList);

        listView.setDivider(getResources().getDrawable(android.R.color.transparent));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),NewsActivity.class);
                intent.putExtra(NewsController.EXTRA_NEWS,mAdapter.getItem(position).getNewsUUID());
                getActivity().startActivity(intent);
            }
        });
        listView.setAdapter(mAdapter);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int lastItem = firstVisibleItem + visibleItemCount;
                if ((lastItem == totalItemCount - 5) && controller.isAvailableOldNews()
                        /*&&!controller.isDownload()*/) {
                    loadNews();
                }
                if ((lastItem <= totalItemCount-1) && !controller.isDBEmpty()) {
                    newsList.addAll(controller.getOlderNews());
                    mAdapter.notifyDataSetChanged();
                    //mAdapter.addAll(controller.getOlderNews());
                }
            }
        });
        if(controller.getNewsList().size() == 0){
            new FirstLoadTask().execute();
        }else{
            newsList.addAll(controller.getNewsList());
            mAdapter.notifyDataSetChanged();
            //mAdapter.clear();
            //mAdapter.addAll(controller.getNewsList());
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        controller.newsReset();
    }

    public void loadNews(){
        controller.loadNews();
    }
    private void initiateRefresh() {
        new UpdateBackgroundTask().execute();
    }

    private void onRefreshComplete(List<News> result) {
        if(result.size() != 0){
            //mAdapter.clear();
            newsList.clear();
            for (News news : result) {
                //mAdapter.add(news);
                newsList.add(news);
            }
            controller.newsReset();
        }
        mAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private class UpdateBackgroundTask extends AsyncTask<Void, Void, List<News>> {

        @Override
        protected List<News> doInBackground(Void... params) {

            if(controller.getServerNews(true))
                return controller.getNewsList();
            return new ArrayList<News>();
        }

        @Override
        protected void onPostExecute(List<News> result) {
            super.onPostExecute(result);
            onRefreshComplete(result);
        }

    }

    private class FirstLoadTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            /*PD = new ProgressDialog(getActivity());
            PD.setTitle("Обновление новостей");
            PD.setMessage("Загрузка...");
            PD.setCancelable(false);
            PD.show();*/
            mSwipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            controller.getServerNews(true);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //mAdapter.clear();
            //mAdapter.addAll(controller.getNewsList());
            newsList.clear();
            newsList.addAll(controller.getNewsList());
            mAdapter.notifyDataSetChanged();
            mSwipeRefreshLayout.setRefreshing(false);
            //PD.dismiss();
        }
    }

    private class  NewsAdapter extends ArrayAdapter<News>{

        ArrayList<News> values;
        Context context;

        public NewsAdapter(Context context, ArrayList<News> values){
            super(context, R.layout.item_news, values);
            this.values = values;
            this.context = context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_news, parent, false);
            }


            TextView textViewTitle = (TextView)convertView.findViewById(R.id.news_title);
            TextView textViewTime = (TextView)convertView.findViewById(R.id.news_time);
            ImageView imageView = (ImageView)convertView.findViewById(R.id.news_logo);

            News news = values.get(position);

            textViewTitle.setText(news.getTitle(80));
            textViewTime.setText(news.getDate());
            if(news.isAction()){
                TextView action  = (TextView)convertView.findViewById(R.id.news_action);
                action.setText("Акция");
                action.setBackgroundResource(R.drawable.partpassed);
                action.setTextColor(getResources().getColor(R.color.partPassed));
            }else{
                TextView action  = (TextView)convertView.findViewById(R.id.news_action);
                action.setText("");
                action.setBackgroundResource(android.R.drawable.screen_background_light_transparent);
                action.setTextColor(getResources().getColor(android.R.color.transparent));
            }
            imageLoader.displayImage(values.get(position).getImg(), imageView,options);

            return convertView;
        }

    }

}