package pro.webformula.dev.CLab.UI;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;

import pro.webformula.dev.CLab.Controllers.FieldValidator;
import pro.webformula.dev.CLab.Controllers.UserProcessedController;
import pro.webformula.dev.CLab.POJO.ValidationModel;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 02.03.2015.
 */
public class UserProcessedActivity extends ActionBarActivity {

    private String[] fields;
    private String[] result;
    private String[] resource;
    private FieldValidator.VALIDATOR_KEY[] fieldValidKey;
    private ValidationModel model;
    private boolean isNewUser;
    private ProcessedArrayAdapter adapter;

    private ProgressDialog progressDialog;
    private ListView listView;

    private UserProcessedController controller;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = UserProcessedController.getController(getApplicationContext());
        isNewUser = getIntent().getBooleanExtra(MainActivity.EXTRA_NEW_USER,true);
        if(!controller.getMail().equals("")&&isNewUser){
            Intent intent = new Intent(getApplicationContext(),ValidationMailActivity.class);
            startActivity(intent);
            finish();
        }

        getActionBar().setIcon(R.drawable.trp);


        if(isNewUser){
            fields = getResources().getStringArray(R.array.reg_array);
            resource = getResources().getStringArray(R.array.img_reg_array);
            getActionBar().setTitle(R.string.registration);
        }
        else{
            fields = getResources().getStringArray(R.array.auth_array);
            resource = getResources().getStringArray(R.array.img_auth_array);
            getActionBar().setTitle("Авторизация");
        }

        result = new String[fields.length];
        fieldValidKey = new FieldValidator.VALIDATOR_KEY[fields.length];
        model = new ValidationModel();

        setKey();
        setContentView(R.layout.activity_processed);
        listView = (ListView)findViewById(R.id.list_view_processed);
        adapter = new ProcessedArrayAdapter(getApplicationContext(),fields);

        listView.setDivider(getResources().getDrawable(android.R.color.transparent));
        listView.setAdapter(adapter);
    }

    public void setKey(){
        for(int i = 0; i<fieldValidKey.length;i++){
            fieldValidKey[i] = FieldValidator.VALIDATOR_KEY.PASS;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menu.add(0, R.menu.menu_main, 1, R.string.yes)
                .setIcon(R.drawable.ok)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        action();
                        return false;
                    }
                })
                .setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void action(){
        if(prepareModel()){
            HashMap<Integer,FieldValidator.VALIDATOR_KEY> resultValidation = FieldValidator.checkValueByKey(sendOnValidate());
            if(isValidResult(resultValidation)){
                int code = -1;
                ActionTask task = new ActionTask();
                task.execute();
                /*try {
                    code = task.get();
                }catch (Exception ex){
                    Log.e("Run", ex.getLocalizedMessage());
                }
                task.cancel(true);
                if(code != -1){
                    if(isNewUser)
                        registration(code);
                    else
                        authorization(code);
                }else{
                    Toast.makeText(getApplicationContext(),"Ошибка соединения",Toast.LENGTH_SHORT).show();
                }*/
            }
        }
    }

    public void registration(int code){
        switch (code){
            case 0:
                controller.setMail(model.getValue(3));
                Intent intent = new Intent(getApplicationContext(),ValidationMailActivity.class);
                startActivity(intent);
                finish();
                break;
            case 1201:
                fieldValidKey[3] = FieldValidator.VALIDATOR_KEY.MAIL;
                if(isVisibleItem(currentVisiblePosition(3)))
                    setErrorInputField(3);
                Toast.makeText(getApplicationContext(),"Почта уже зарегестрирована",Toast.LENGTH_SHORT).show();
                break;
            case 1202:
                if(isVisibleItem(currentVisiblePosition(4)))
                    setErrorInputField(4);
                Toast.makeText(getApplicationContext(),"Слишком короткий пароль, минимум 6 символов",Toast.LENGTH_SHORT).show();
                break;

        }
    }

    public void authorization(int code){
        switch (code){
            case 0:
                controller.setLogin(true);
                Intent intent = new Intent(getApplicationContext(),ParentActivity.class);
                startActivity(intent);
                finish();
                break;
            case 1001:
                setErrorInputField(0);
                setErrorInputField(1);
                fieldValidKey[1] = FieldValidator.VALIDATOR_KEY.ONLYTEXT;
                Toast.makeText(getApplicationContext(),"Неверные данные авторизации",Toast.LENGTH_SHORT).show();
                break;

        }
    }

    public boolean prepareModel(){
        boolean isPrepare = true;
        for(int i = 0;i<fieldValidKey.length;i++){
            if(model.keyContain(i)){
                if(!model.isEmptyField(i)){
                    if((fieldValidKey[i] ==FieldValidator.VALIDATOR_KEY.PASS)
                            || (fieldValidKey[i] ==FieldValidator.VALIDATOR_KEY.NOTNULL))
                        fieldValidKey[i] = FieldValidator.VALIDATOR_KEY.PASS;
                }else{
                    fieldValidKey[i] = FieldValidator.VALIDATOR_KEY.NOTNULL;
                    isPrepare = false;
                    if(isVisibleItem(currentVisiblePosition(i)))
                        setErrorEmptyField(currentVisiblePosition(i));
                }
            }else{
                fieldValidKey[i] = FieldValidator.VALIDATOR_KEY.NOTNULL;
                isPrepare = false;
                Toast.makeText(getApplicationContext(),"Заполните все поля",Toast.LENGTH_SHORT).show();
            }
        }
        return isPrepare;
    }

    public int currentVisiblePosition(int position){
        return position - listView.getFirstVisiblePosition();
    }

    public boolean isVisibleItem(int position){
        if((position >= 0)&&(position <= listView.getChildCount()-1)){
            return true;
        }
        return false;
    }

    public EditText getValidateField(int position){
        View view = listView.getChildAt(position);
        return (EditText) view.findViewById(R.id.field);
    }
    public void setErrorEmptyField(int position){
        EditText editText =(getValidateField(position));
        editText.setHintTextColor(getResources().getColor(android.R.color.holo_red_light));
    }

    public void setErrorInputField(int position){
        EditText editText =(getValidateField(position));
        editText.setTextColor(getResources().getColor(android.R.color.holo_red_light));
    }

    public HashMap<Integer,HashMap<String,FieldValidator.VALIDATOR_KEY>> sendOnValidate(){
        HashMap<Integer,HashMap<String,FieldValidator.VALIDATOR_KEY>> temp
                = new HashMap<Integer, HashMap<String, FieldValidator.VALIDATOR_KEY>>();
        for(int position = 0;position<fields.length;position++){
            HashMap<String,FieldValidator.VALIDATOR_KEY> innerTemp
                    = new HashMap<String, FieldValidator.VALIDATOR_KEY>();
            innerTemp.put(model.getValue(position), getValidationKey(position));
            temp.put(position, innerTemp);
        }
        return temp;
    }

    public boolean isValidResult(HashMap<Integer,FieldValidator.VALIDATOR_KEY> mapValidation){
        if(mapValidation.size()!=0){
            for(Integer position:mapValidation.keySet()){
                /*switch (mapValidation.get(position)){
                    case DATE:
                        if(isVisibleItem(currentVisiblePosition(position))){
                            setErrorInputField(currentVisiblePosition(position));
                        }
                        fieldValidKey[position] = FieldValidator.VALIDATOR_KEY.DATE;
                        break;
                    default:
                        if(isVisibleItem(currentVisiblePosition(position))){
                            setErrorInputField(currentVisiblePosition(position));
                        }
                        fieldValidKey[position] = FieldValidator.VALIDATOR_KEY.ONLYTEXT;
                        break;
                }*/
                if(isVisibleItem(currentVisiblePosition(position))){
                    setErrorInputField(currentVisiblePosition(position));
                }
                fieldValidKey[position] = mapValidation.get(position);
                break;
            }
            return false;
        }else{
            return true;
        }
    }

    private class ProcessedArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        public ProcessedArrayAdapter(Context context, String[] values) {
            super(context, R.layout.item_field, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.item_field, parent, false);

            EditText editText = (EditText) rowView.findViewById(R.id.field);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.img);

            validateText(editText,position);

            setTextType(editText,position);

            editText.setHint(values[position]);
            editText.addTextChangedListener(new Watcher(editText, position));

            int resId = getResources().getIdentifier(resource[position],"drawable",getPackageName());
            imageView.setImageResource(resId);

            if(!editText.getText().toString().equals("")){
                result[position] = editText.getText().toString();
            }else{
                editText.setText(result[position]);
            }

            return rowView;
        }

    }

    public void validateText(EditText editText,int position){
        switch (fieldValidKey[position]){
            case NOTNULL:
                editText.setHintTextColor(getResources().getColor(android.R.color.holo_red_light));
                break;
            case PASS:
                break;
            default:
                editText.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                break;
        }
    }

    public void setTextType(EditText editText,int position){
        editText.setTextColor(getResources().getColor(android.R.color.black));
        editText.setHintTextColor(getResources().getColor(android.R.color.darker_gray));
        if(isNewUser){
            switch (position){
                case 4:
                    break;
                case 9:
                    editText.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;
                case 5:
                    editText.setFocusable(false);
                    editText.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
                    editText.setOnClickListener(
                            new UserClickListener(editText, UserClickListener.DIALOG_DATE));
                    break;
                case 6:
                    editText.setFocusable(false);
                    editText.setOnClickListener(
                            new UserClickListener(editText, UserClickListener.DIALOG_GENDER));
                    editText.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                    break;
                case 10:
                    editText.setInputType(InputType.TYPE_CLASS_PHONE);
                    break;
                case 11:
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    break;
                default:
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                    break;
            }
        }else{
            switch (position){
                case 1:
                    break;
                default:
                    editText.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                    break;
            }
        }
    }

    public FieldValidator.VALIDATOR_KEY getValidationKey(int position){
        if(isNewUser){
            switch (position){
                case 3:
                    return FieldValidator.VALIDATOR_KEY.MAIL;
                case 4:
                    return FieldValidator.VALIDATOR_KEY.NOTNULL;
                case 5:
                    return FieldValidator.VALIDATOR_KEY.DATE;
                case 6:
                    return FieldValidator.VALIDATOR_KEY.NOTNULL;
                case 9:
                    return FieldValidator.VALIDATOR_KEY.NOTNULL;
                case 10:
                    return FieldValidator.VALIDATOR_KEY.PHONE;
                case 11:
                    return FieldValidator.VALIDATOR_KEY.NOTNULL;
                default:
                    return FieldValidator.VALIDATOR_KEY.ONLYTEXT;
            }
        }else{
            switch (position){
                case 0:
                    return FieldValidator.VALIDATOR_KEY.MAIL;
                default:
                    return FieldValidator.VALIDATOR_KEY.NOTNULL;
            }
        }
    }


    private class Watcher implements TextWatcher {

        protected EditText linkedView;
        private int position;

        public Watcher(EditText linkedView,int position) {
            this.linkedView = linkedView;
            this.position = position;
        }

        public void afterTextChanged(Editable s) {
            if(s.length()>=0){
                result[position] = s.toString();
                model.setValidationField(position,s.toString());
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            linkedView.setTextColor(getResources().getColor(android.R.color.black));
        }
    }

    public class UserClickListener implements View.OnClickListener{
        public static final int DIALOG_DATE = 0;
        public static final int DIALOG_GENDER = 1;
        private EditText editText;
        private int dialog;

        UserClickListener(EditText editText, int dialog){
            this.dialog = dialog;
            this.editText = editText;
        }

        @Override
        public void onClick(View v) {
            switch (dialog){
                case DIALOG_DATE:
                    showDatePickerDialog(editText);
                    break;
                case DIALOG_GENDER:
                    showGenderPickerDialog(editText);
                    break;
                default:
                    break;
            }
        }
    }


    public void showDatePickerDialog(EditText editText){
        DatePickerFragment dataPicker = new DatePickerFragment();
        dataPicker.setEditTextElement(editText);
        dataPicker.show(getSupportFragmentManager(), "datePicker");
    }

    public void showGenderPickerDialog(final EditText editText){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.gender)
                .setItems(R.array.genders, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0){
                            editText.setText(R.string.men);
                        }else{
                            editText.setText(R.string.women);
                        }
                    }
                });
        builder.show();
    }

    private class ActionTask extends AsyncTask <Void,Void,Integer>{

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            progressDialog = new ProgressDialog(UserProcessedActivity.this);
            progressDialog.setTitle("Связь с сервером");
            progressDialog.setMessage("Синхронизация...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            int res;
            if(isNewUser)
                res = controller.sendValidationData(model,getResources().getStringArray(R.array.json_reg),isNewUser);
            else
                res = controller.sendValidationData(model,getResources().getStringArray(R.array.json_auth),isNewUser);
            Log.e("res","res "+res);
            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if(result != -1){
                if(isNewUser)
                    registration(result);
                else
                    authorization(result);
            }else{
                Toast.makeText(getApplicationContext(),"Ошибка соединения",Toast.LENGTH_LONG).show();
            }
            progressDialog.dismiss();
        }
    }
}
