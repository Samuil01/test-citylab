package pro.webformula.dev.CLab.POJO;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Created by dev on 12.02.2015.
 */
public class News{

    private String newsUUID;
    private String title;
    private String img;
    private String description;
    private Date date;
    private boolean isAction;


    private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm");


    public String getNewsUUID() {
        return newsUUID;
    }

    public void setUUID(String uuid) { this.newsUUID = uuid;}

    public String getTitle() {
        return title;
    }

    public String getTitle(int length) {
        return cutTitle(length,title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String cutTitle(int length, String title){
        if(title.length() > length){
            int i = length-1;
            char[] arrayString = new char[length];
            title.getChars(0,80,arrayString,0);
            while(arrayString[i]!=' '){
                i--;
            }
            for(int j = 0; j < 3;j++){
                arrayString[i] = '.';
                i++;
            }
            for(int j = i;j<length;j++){
                arrayString[j] = ' ';
            }
            title = new String(arrayString,0,length);
        }
        return title;
    }

    public boolean isAction() {
        return isAction;
    }

    public void setAction(boolean isAction) {
        this.isAction = isAction;
    }
}