package pro.webformula.dev.CLab;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import pro.webformula.dev.CLab.Controllers.FilialAgencyController;
import pro.webformula.dev.CLab.Controllers.MedicalTestController;
import pro.webformula.dev.CLab.Controllers.NewsController;
import pro.webformula.dev.CLab.Controllers.OrderController;
import pro.webformula.dev.CLab.POJO.FilialAgency;
import pro.webformula.dev.CLab.POJO.News;

/**
 * Created by dev on 17.02.2015.
 */
public class SqlBaseManager extends SQLiteOpenHelper {

    final String LOG_TAG = "Sql_tag";
    static Context mContext;

    public SqlBaseManager(Context context) {
        super(context, "localDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "--- onCreate database ---");
        String scriptNews= "CREATE TABLE "+ NewsController.HEADER_NEWS +" ("
                + "_id integer primary key autoincrement,"
                + NewsController.TAG_UUID + " text,"
                + NewsController.TAG_CREATED_AT + " integer,"
                + NewsController.TAG_TITLE + " text,"
                + NewsController.TAG_IMG_URL + " text,"
                + NewsController.TAG_TEXT + " text,"
                + NewsController.TAG_IS_ACTION + " text,"
                + NewsController.TAG_DELETED + " text,"
                + NewsController.TAG_UPDATED_AT + " integer"
                +");";
        String scriptFilial = "CREATE TABLE "+ FilialAgencyController.HEADER_LABS +" ("
                + "_id integer primary key autoincrement,"
                + FilialAgencyController.TAG_UUID + " text,"
                + FilialAgencyController.TAG_LATITUDE + " real,"
                + FilialAgencyController.TAG_LONGITUDE + " real,"
                + FilialAgencyController.TAG_WEB_SITE + " text,"
                + FilialAgencyController.TAG_WORK_TIME + " text,"
                + FilialAgencyController.TAG_PHONE_NUMBER + " text,"
                + FilialAgencyController.TAG_ADDRESS + " text,"
                + FilialAgencyController.TAG_LOCATION + " text,"
                + FilialAgencyController.TAG_REGION + " text,"
                + FilialAgencyController.TAG_DELETED + " text,"
                + FilialAgencyController.TAG_UPDATED_AT + " integer"
                +");";

        String scriptAnalyzes = "CREATE TABLE "+ MedicalTestController.HEADER_ANALYZES +" ("
                + "_id integer primary key autoincrement,"
                + MedicalTestController.TAG_UUID + " text,"
                + MedicalTestController.TAG_NAME + " text,"
                + MedicalTestController.TAG_PRICE + " real,"
                + MedicalTestController.TAG_DURATION + " integer,"
                + MedicalTestController.TAG_NUMBER + " text,"
                + MedicalTestController.TAG_ICON+ " text,"
                + MedicalTestController.TAG_BRIEF_DESC + " text,"
                + MedicalTestController.TAG_FULL_DESC + " text,"
                + MedicalTestController.TAG_ADDITIONAL_DESC + " text,"
                + MedicalTestController.TAG_CATEGORY + " text,"
                + MedicalTestController.TAG_DELETED + " text,"
                + MedicalTestController.TAG_UPDATED_AT + " integer"
                +");";

        String scriptOrders = "CREATE TABLE "+ OrderController.HEADER_REQUESTS +" ("
                + "_id integer primary key autoincrement,"
                + OrderController.TAG_UUID + " text,"
                + OrderController.TAG_DOCTOR_ID + " text,"
                + OrderController.TAG_LAST_NAME + " text,"
                + OrderController.TAG_FIRST_NAME + " text,"
                + OrderController.TAG_SECOND_NAME + " text,"
                + OrderController.TAG_DATE_OF_BIRTH + " integer,"
                + OrderController.TAG_GENDER + " int,"
                + OrderController.TAG_PHONE_NUMBER + " text,"
                + OrderController.TAG_PREGNANCY + " integer,"
                + OrderController.TAG_PREGNANCY_PERIOD + " integer,"
                + OrderController.TAG_STATUS + " integer,"
                + OrderController.TAG_ANALYSIS_ID + " text,"
                + OrderController.TAG_CREATED_AT + " integer,"
                + OrderController.TAG_MENOPAUSE + " text"
                +");";
        db.execSQL(scriptNews);
        db.execSQL(scriptFilial);
        db.execSQL(scriptAnalyzes);
        db.execSQL(scriptOrders);

        Log.d(LOG_TAG, "--- onCreate database all table ---");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void clearTable(String table){
        this.getReadableDatabase().delete(table,null,null);
        Log.e("Clear","table "+table);
        Cursor cursor = this.getReadableDatabase().query(table,null,null,null,null,null,null);
        Log.e("SizeTable","Size of table"+cursor.getCount());
    }

    public long getNewerDate(String table, String tag){
        Cursor cursor = this.getReadableDatabase().query(table,null,null,null,null,null,tag);
        if(cursor.getCount()>0){
            if(!cursor.isLast())
                cursor.moveToLast();
            return cursor.getLong(cursor.getColumnIndex(tag));
        }else{
            return 0;
        }
    }
    public long getOlderDate(String table, String tag){
        Cursor cursor = this.getReadableDatabase().query(table,null,null,null,null,null,tag+" DESC");
        if(cursor.getCount()>0){
            if(!cursor.isLast())
                cursor.moveToLast();
            return cursor.getLong(cursor.getColumnIndex(tag));
        }else{
            return 0;
        }
    }

    public News getNewsFromDB(String uuid){
        News news = new News();
        Cursor cursor = this.getReadableDatabase().query(NewsController.HEADER_NEWS,null,null,null,null,null,null);
        if(cursor.moveToFirst()){
            int id = cursor.getColumnIndex(NewsController.TAG_UUID);
            int title = cursor.getColumnIndex(NewsController.TAG_TITLE);
            int date = cursor.getColumnIndex(NewsController.TAG_CREATED_AT);
            int img = cursor.getColumnIndex(NewsController.TAG_IMG_URL);
            int text = cursor.getColumnIndex(NewsController.TAG_TEXT);
            do{
                if(!cursor.getString(id).equals(uuid))
                    continue;
                news.setUUID(cursor.getString(id));
                news.setTitle(cursor.getString(title));
                news.setDate((new Date(cursor.getLong(date) * 1000)));
                news.setImg(cursor.getString(img));
                news.setDescription(cursor.getString(text));
                Log.e("NewsDB","found");
                break;
            }while (cursor.moveToNext());
        }else{
            Log.e("NewsDB","wrong query");
        }
        return news;
    }


    public boolean isEqualResult(String table, String tag, String value){
        String selection = tag+" = ?";
        String[] selectionArgs = new String[] { value };
        Cursor cursor = this.getReadableDatabase().query(table,null,selection,selectionArgs,null,null,null);
        if(cursor.getCount()==1){
            Log.e("Equal","UUID "+value);
            return true;
        }
        Log.e("Equal","unequal "+value);
        return false;
    }

    public void deleteOrder(String uuid){
        String[] args = {uuid};
        long res = this.getWritableDatabase().delete(OrderController.HEADER_REQUESTS,"uuid = ?",args);
        Log.e("delete","row "+res);
    }

}
