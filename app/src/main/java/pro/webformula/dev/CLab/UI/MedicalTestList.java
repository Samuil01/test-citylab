package pro.webformula.dev.CLab.UI;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;

import pro.webformula.dev.CLab.Controllers.FilialAgencyController;
import pro.webformula.dev.CLab.POJO.FilialAgency;
import pro.webformula.dev.CLab.POJO.MedicalTest;
import pro.webformula.dev.CLab.Controllers.MedicalTestController;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 18.02.2015.
 */
public class MedicalTestList extends Activity {

    private ArrayList<MedicalTest> tests;
    private ArrayList<Integer> testId;

    private TextView textViewCountTest;
    private ImageView imageViewConfirmTest;
    private ImageView imageViewSearch;
    private ImageView imageViewBack;
    private ExpListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_test);

        testId = new ArrayList<Integer>();

        ExpandableListView listView = (ExpandableListView)findViewById(R.id.test_expandable_list);

        HashMap<String,ArrayList<MedicalTest>> groups = MedicalTestController.getController(getApplicationContext()).getArrayTestType();
        adapter = new ExpListAdapter(getApplicationContext(), groups);
        listView.setAdapter(adapter);

        textViewCountTest = (TextView)findViewById(R.id.item_test_count);
        textViewCountTest.setText(""+testId.size());

        imageViewSearch = (ImageView)findViewById(R.id.test_item_search);
        imageViewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                intent.putExtra(SearchActivity.SEARCH_TYPE,2);
                startActivityForResult(intent, 1);
            }
        });

        imageViewConfirmTest = (ImageView)findViewById(R.id.test_item_confirm);
        imageViewConfirmTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getResultId() == null) finish();
                Intent intent = getIntent();
                intent.putExtra("id",getResultId());
                setResult(RESULT_OK,intent);
                finish();
            }
        });

        imageViewBack = (ImageView)findViewById(R.id.item_test_list_back);
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        getActionBar().hide();
    }

    public int[] getResultId(){
        if(testId.size() == 0) return  null;
        int[] res = new int[testId.size()];
        int iteration = 0;
        for(Integer id : testId){
            res[iteration] = id;
            iteration++;
        }
        return res;
    }

    class ExpListAdapter extends BaseExpandableListAdapter {

        private HashMap<String,ArrayList<MedicalTest>> test;
        private ArrayList<String> handlerGroup;
        private Context context;

        public ExpListAdapter (Context context,HashMap<String,ArrayList<MedicalTest>> test){
            this.context = context;
            this.test = test;
            handlerGroup = new ArrayList<String>();
            for(String type: test.keySet()){
                handlerGroup.add(type);
            }
        }

        @Override
        public int getGroupCount() {
            return test.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return test.get(((String)getGroup(groupPosition))).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return handlerGroup.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return test.get((String)getGroup(groupPosition)).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return ((MedicalTest)getChild(groupPosition,childPosition)).getId();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                                 ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.expandbale_list_item, null);
            }

            TextView textGroup = (TextView) convertView.findViewById(R.id.expand_test_type);
            textGroup.setText((String)getGroup(groupPosition));

            return convertView;

        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild,
                                 View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_medical_test, null);
            }

            MedicalTest medicalTest = (MedicalTest)getChild(groupPosition,childPosition);

            ImageView imageViewIcon = (ImageView)convertView.findViewById(R.id.item_test_img);
            TextView textViewTitle = (TextView)convertView.findViewById(R.id.item_test_title);
            TextView textViewDay = (TextView)convertView.findViewById(R.id.item_test_day);
            TextView textViewCost = (TextView)convertView.findViewById(R.id.item_test_cost);
            TextView textViewId = (TextView)convertView.findViewById(R.id.item_test_id);
            ImageView imageViewSelect = (ImageView)convertView.findViewById(R.id.item_test_selected);

            textViewTitle.setText(medicalTest.getName());
            textViewDay.setText(medicalTest.getTime(1));
            textViewCost.setText(medicalTest.getPrice(true));
            textViewId.setText(medicalTest.getNumber());

            int idIcon = getResources().getIdentifier(medicalTest.getImg(),
                    "drawable", context.getPackageName());
            imageViewIcon.setImageResource(idIcon);
            if(testId.contains(medicalTest.getId())){
                imageViewSelect.setImageResource(R.drawable.active);
            }else{
                imageViewSelect.setImageResource(R.drawable.noactive);
            }

            imageViewSelect.setOnClickListener(new View.OnClickListener() {
                int group = groupPosition;
                int id = childPosition;
                ImageView imageViewSelect;

                @Override
                public void onClick(View v) {
                    if (testId.contains(((MedicalTest) getChild(group, id)).getId())) {
                        imageViewSelect = (ImageView) v.findViewById(R.id.item_test_selected);
                        imageViewSelect.setImageResource(R.drawable.noactive);
                        onUnSelectItem((int)getChildId(group,id));
                    } else {
                        imageViewSelect = (ImageView) v.findViewById(R.id.item_test_selected);
                        imageViewSelect.setImageResource(R.drawable.active);
                        onSelectItem((int)getChildId(group,id));
                    }
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String desc = ((MedicalTest) getChild(groupPosition, childPosition)).getDesk();
                    if(!"".equals(desc))
                        showWebViewDialog(desc);
                }
            });

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
        public void onSelectItem(int i){
            testId.add(new Integer(i));
            textViewCountTest.setText(""+testId.size());
        }

        public void onUnSelectItem(int i){
            testId.remove(new Integer(i));
            textViewCountTest.setText(""+testId.size());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        int[] ids = data.getIntArrayExtra("idTests");
        if(ids==null)
            return;
        for (int id : ids) {
            if (!testId.contains(id)) {
                testId.add(id);
            }
        }
        textViewCountTest.setText(""+testId.size());
    }

    public void showWebViewDialog(String desc){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Анализ");



        String str = "<html><body>You scored <b>192</b> points.</body></html>";

//      wv.loadUrl(desc);
        if("".equals(desc) || desc == null){
            alert.setMessage("Раздел находится в наполнении");
        } else {
            WebView wv = new WebView(this);
            wv.loadData(desc, "text/html; charset=UTF-8", null);
            wv.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
            alert.setView(wv);
        }

        alert.setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alert.show();
    }
}
