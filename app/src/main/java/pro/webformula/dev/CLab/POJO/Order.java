package pro.webformula.dev.CLab.POJO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 13.02.2015.
 */
public class Order {

    public static enum statusOrder {local,wait,inProgress,isDone,partImplemented,partPassed,deleted};
    public static enum isHave {Yes,No,Maybe};
    private static SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy");

    private String orderUUID;
    private Customer customer;
    private statusOrder status;
    private double cost;
    private Date dateCreate;
    private ArrayList<MedicalTest> testsList;
    private boolean pause;
    private Pregnancy pregnancy = new Pregnancy();
    private String searchString;

    public String getOrderUUID() {
        return orderUUID;
    }

    public void setOrderUUID(String orderUUID) {
        this.orderUUID = orderUUID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        searchString += customer.getSearchString();
    }

    public statusOrder getStatus() {
        return status;
    }

    public int getStatus(boolean i) {
        switch (status){
            case local:
                return 0;
            case wait:
                return 1;
            case inProgress:
                return 2;
            case isDone:
                return 3;
            case partImplemented:
                return 4;
            case partPassed:
                return 5;
            case deleted:
                return 6;
        }
        return -1;
    }

    public String getStatusString() {
        switch (status){
            case local:
                return "Не отправлена";
            case wait:
                return "Ожидает оплаты";
            case inProgress:
                return "В работе";
            case isDone:
                return "Выплонено";
            case partImplemented:
                return "Вып. частично";
            case partPassed:
                return "Сданы частично";
            case deleted:
                return "Неявка 30 дней";
        }
        return "";
    }

    public void setStatus(statusOrder status) {
        this.status = status;
    }

    public void setStatus(int status) {
        switch (status){
            case 0:
                this.status = statusOrder.local;
                break;
            case 1:
                this.status = statusOrder.wait;
                break;
            case 2:
                this.status = statusOrder.inProgress;
                break;
            case 3:
                this.status = statusOrder.isDone;
                break;
            case 4:
                this.status = statusOrder.partImplemented;
                break;
            case 5:
                this.status = statusOrder.partPassed;
                break;
            case 6:
                this.status = statusOrder.deleted;
                break;
        }
        searchString+=getStatusString();
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDateCreate() {
        return format.format(dateCreate);
    }
    public Long getDateCreateUnixTime() {
        return dateCreate.getTime()/1000;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public ArrayList<MedicalTest> getTestsList() {
        return testsList;
    }

    public void setTestsList(ArrayList<MedicalTest> testsList) {
        for(MedicalTest test:testsList){
            this.setCost(this.getCost()+test.getPrice());
        }
        this.testsList = testsList;
    }

    public boolean isPause() {
        return pause;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }

    public Pregnancy getPregnancy() {
        return pregnancy;
    }

    public void setPregnancy(isHave isPregnancy, int date) {
        this.pregnancy.setChildbearing(isPregnancy);
        this.pregnancy.setPossibleDate(date);
    }

    public String getSearchString(){
        return searchString;
    }

    public String[] getTestTitle(){
        int all = testsList.size();
        String list = "";
        for(MedicalTest test:testsList){
            list += test.getName()+" ";
        }
        return new String[]{""+all, list};
    }

    public String getTestUUID(){
        String list = "";
        for(MedicalTest test:testsList){
            list += test.getName()+"|";
        }
        return list;
    }

    public String getCostTitle(){
        String result = "";
        if(cost/1000>0){
            if((int)cost/1000 != 0){
                result+=(int)cost/1000;
                result+=" ";
            }
            if(cost%1000>100){
                result+=cost%1000;
            }
            else{
                if(cost%1000<10){
                    result+="00"+cost%1000;
                }else{
                    result+="0"+cost%1000;
                }
            }

        }else{
            result += cost;
        }
        return  result;
    }

    public class Pregnancy{

        private isHave childbearing;
        private int possibleDate;

        public isHave getChildbearing() {
            return childbearing;
        }

        public void setChildbearing(isHave childbearing) {
            this.childbearing = childbearing;
        }

        public int getPossibleDate() {
            return possibleDate;
        }

        public void setPossibleDate(int possibleDate) {
            this.possibleDate = possibleDate;
        }
    }


}
