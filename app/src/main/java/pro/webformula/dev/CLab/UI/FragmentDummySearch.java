package pro.webformula.dev.CLab.UI;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 27.02.2015.
 */
public class FragmentDummySearch extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dummy_search, null);
        TextView textViewSearch = (TextView)view.findViewById(R.id.search_text_type);
        int type = getArguments().getInt(SearchActivity.SEARCH_TYPE,0);
        String[] typeArray = getResources().getStringArray(R.array.search_text);
        if(type <= typeArray.length){
            textViewSearch.setText(typeArray[type]);
        }
        return view;
    }
}
