package pro.webformula.dev.CLab.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import pro.webformula.dev.CLab.POJO.FilialAgency;
import pro.webformula.dev.CLab.SqlBaseManager;

/**
 * Created by dev on 16.02.2015.
 */
public class FilialAgencyController implements Controllers{


    private static final String APP_PREFERENCES = "CityLab";
    public static final String APP_PREFERENCES_COOKIE = "Cookie";

    public static final String SERVER_URL ="http://195.128.126.168:84/api/core.ashx";
    public static final String HEADER_NAME_MODIFI = "If-Modified-Since";
    public static final String HEADER_NAME_X_COMAND = "X-Command";
    public static final String HEADER_LABS = "Labs";

    public static final String TAG_UUID = "uuid";
    public static final String TAG_LATITUDE = "latitude";
    public static final String TAG_LONGITUDE = "longitude";
    public static final String TAG_PHONE_NUMBER = "phone_number";
    public static final String TAG_WEB_SITE = "web_site";
    public static final String TAG_DELETED = "deleted";
    public static final String TAG_UPDATED_AT = "updated_at";
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_LOCATION = "location";
    public static final String TAG_REGION = "region";
    public static final String TAG_WORK_TIME = "work_time";

    private ArrayList<FilialAgency> filialAgencies;
    private ArrayList<String> group;
    private static FilialAgencyController controller;
    private static Context context;

    private static SqlBaseManager manager;
    private static SharedPreferences sharedPreferences;

    private FilialAgencyController(){
        manager = new SqlBaseManager(context);
        sharedPreferences = context.getApplicationContext()
                .getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        filialAgencies = new ArrayList<FilialAgency>();
        group = new ArrayList<String>();
        if(manager.getNewerDate(HEADER_LABS,TAG_UPDATED_AT)!=0){
            init();
        }
    }

    public static FilialAgencyController getController(Context currContext){
        if(controller == null){
            context = currContext;
            controller = new FilialAgencyController();
        }
        return controller;
    }

    public ArrayList<FilialAgency> getFilialAgencies(){
        return filialAgencies;
    }

    public FilialAgency getAgency(String uuid){
        for(FilialAgency agency:filialAgencies){
            if(agency.getUUID().equals(uuid)){
                return agency;
            }
        }
        return null;
    }

    public void init(){
        SQLiteDatabase base = manager.getReadableDatabase();
        String selection = TAG_DELETED+" = ?";
        String[] selectionArgs = new String[] { "false" };
        Cursor cursor = base.query(HEADER_LABS,null,selection,selectionArgs,null,null,null);
        if(cursor.moveToFirst()){
            filialAgencies.clear();
            int uuid = cursor.getColumnIndex(TAG_UUID);
            int latitude = cursor.getColumnIndex(TAG_LATITUDE);
            int longitude = cursor.getColumnIndex(TAG_LONGITUDE);
            int webSite = cursor.getColumnIndex(TAG_WEB_SITE);
            int workTime = cursor.getColumnIndex(TAG_WORK_TIME);
            int phoneNumber = cursor.getColumnIndex(TAG_PHONE_NUMBER);
            int address = cursor.getColumnIndex(TAG_ADDRESS);
            int location  = cursor.getColumnIndex(TAG_LOCATION);
            int region = cursor.getColumnIndex(TAG_REGION);
            do{
                FilialAgency filialAgency = new FilialAgency();
                filialAgency.setUUID(cursor.getString(uuid));
                filialAgency.setCoordinateX(cursor.getDouble(latitude));
                filialAgency.setCoordinateY(cursor.getDouble(longitude));
                filialAgency.setSite(cursor.getString(webSite));
                filialAgency.setPhone(cursor.getString(phoneNumber));
                filialAgency.setAddress(cursor.getString(address));
                filialAgency.setLocation(cursor.getString(location));
                filialAgency.setRegion(cursor.getString(region));
                filialAgency.setTime(cursor.getString(workTime));
                /*filialAgency.setMapAddress(getAddressFromLatLng(filialAgency.getCoordinateX(),
                        filialAgency.getCoordinateY()));*/
                //new ShadowTask().execute(filialAgency);
                filialAgencies.add(filialAgency);
            }while (cursor.moveToNext());
        }
        initGroup();
    }

    public void initGroup(){
        for(FilialAgency filialAgency:filialAgencies){
            if(!group.contains(filialAgency.getLocation()))
                group.add(filialAgency.getLocation());
        }
    }

    public HashMap<String,ArrayList<FilialAgency>> getArrayFilialRegion(){
        HashMap<String,ArrayList<FilialAgency>> arraySortedFilialOnRegion = new HashMap<String,ArrayList<FilialAgency>>();
        for(String region: group){
            ArrayList<FilialAgency> agencies = new ArrayList<FilialAgency>();
            for(FilialAgency agency : filialAgencies){
                if(region.equals(agency.getLocation())){
                    agencies.add(agency);
                }
            }
            arraySortedFilialOnRegion.put(region, agencies);
        }
        return arraySortedFilialOnRegion;
    }

    @Override
    public ArrayList<Object> getSearchResult(String query) {
        ArrayList<Object> result = new ArrayList<Object>();

        for(FilialAgency filial : filialAgencies){
            if(filial.getAddress().toLowerCase().contains(query)){
                result.add(filial);
            }
        }
        return result;
    }

    public boolean getServerFilial(){
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet();
        //context.deleteDatabase("localDB");
        Long modifyDate = manager.getNewerDate(HEADER_LABS, TAG_UPDATED_AT);
        String modify = ""+modifyDate;
        httpGet.setHeader(HEADER_NAME_MODIFI, modify);
        httpGet.setURI(URI.create(SERVER_URL));
        String cookie = sharedPreferences.getString(APP_PREFERENCES_COOKIE, "");
        httpGet.setHeader(APP_PREFERENCES_COOKIE,cookie);
        httpGet.setHeader(HEADER_NAME_X_COMAND, HEADER_LABS);
        try{
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");

            HttpResponse response = defaultHttpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 200){
                String line = EntityUtils.toString(response.getEntity());
                parseFromJson(line);
                init();
                return true;
            }
            if(statusCode == 304){
                Log.e("Data","No data to update ");
                return false;
            }
        }catch (IOException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
        return false;
    }
    private void parseFromJson(String response){
        try {
            JSONArray jsonArray = new JSONArray(response);
            for(int i = 0; i < jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                parseToBD(jsonObject);
            }
        }catch (Exception ex){
            Log.e("JSON",ex.getLocalizedMessage());
        }
    }
    private void parseToBD(JSONObject jsonObject){

        SQLiteDatabase writableDatabase = manager.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        try {
            String uuid = jsonObject.getString(TAG_UUID);
            contentValues.put(TAG_UUID, uuid);
            contentValues.put(TAG_LATITUDE, jsonObject.getDouble(TAG_LATITUDE));
            contentValues.put(TAG_LONGITUDE, jsonObject.getDouble(TAG_LONGITUDE));
            contentValues.put(TAG_PHONE_NUMBER, jsonObject.getString(TAG_PHONE_NUMBER));
            contentValues.put(TAG_WORK_TIME, jsonObject.getString(TAG_WORK_TIME));
            contentValues.put(TAG_WEB_SITE, jsonObject.getString(TAG_WEB_SITE));
            contentValues.put(TAG_ADDRESS, jsonObject.getString(TAG_ADDRESS));
            contentValues.put(TAG_LOCATION, jsonObject.getString(TAG_LOCATION));
            contentValues.put(TAG_REGION, jsonObject.getString(TAG_REGION));
            contentValues.put(TAG_DELETED, jsonObject.getString(TAG_DELETED));
            contentValues.put(TAG_UPDATED_AT, jsonObject.getLong(TAG_UPDATED_AT));
            if(!manager.isEqualResult(HEADER_LABS,TAG_UUID,uuid)){
                long id = writableDatabase.insert(HEADER_LABS, null, contentValues);
                Log.e("JSON_BD","insert "+id+" ad "+jsonObject.getString(TAG_LOCATION));
            }else{
                long id = writableDatabase.update(HEADER_LABS, contentValues,TAG_UUID+" = ?",new String[]{uuid});
                Log.e("JSON_BD","update "+id+" ad "+jsonObject.getString(TAG_LOCATION));
            }
        }catch (JSONException ex){
            Log.e("JSON",ex.getLocalizedMessage());
        }
    }
    public Address getAddressFromLatLng(double lat, double lng){
        Geocoder geo = new Geocoder(context,Locale.getDefault());
        try {
            List<Address> listAddresses = geo.getFromLocation(lat, lng, 1);
            if(null!=listAddresses&&listAddresses.size()>0) {
                return listAddresses.get(0);
            }
        }catch (IOException ex){
            Log.e("GEO",ex.getLocalizedMessage());
            return getAddressFromLatLng(lat, lng);
        }
        return null;
    }

    private class ShadowTask extends AsyncTask<FilialAgency,Void,Void> {

        @Override
        protected Void doInBackground(FilialAgency... params) {
            params[0].setMapAddress(getAddressFromLatLng(params[0].getCoordinateX(),params[0].getCoordinateY()));
            Log.e("SHADOW","set "+params[0].getMapAddress().getThoroughfare());
            return null;
        }
    }
}
