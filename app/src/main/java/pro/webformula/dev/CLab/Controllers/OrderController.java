package pro.webformula.dev.CLab.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import pro.webformula.dev.CLab.POJO.Customer;
import pro.webformula.dev.CLab.POJO.MedicalTest;
import pro.webformula.dev.CLab.POJO.Order;
import pro.webformula.dev.CLab.SqlBaseManager;

/**
 * Created by dev on 13.02.2015.
 */
public class OrderController implements Controllers {

    private static final String APP_PREFERENCES = "CityLab";
    public static final String APP_PREFERENCES_COOKIE = "Cookie";

    public static final String SERVER_URL ="http://195.128.126.168:84/api/core.ashx";
    public static final String HEADER_NAME_MODIFI = "If-Modified-Since";
    public static final String HEADER_NAME_X_COMAND = "X-Command";
    public static final String HEADER_REQUESTS = "Requests";

    public static final String TAG_UUID = "uuid";

    public static final String TAG_DOCTOR_UUID = "doctor_uuid";
    public static final String TAG_DOCTOR_ID = "doctor_id";
    public static final String TAG_LAST_NAME = "last_name";
    public static final String TAG_FIRST_NAME = "first_name";
    public static final String TAG_SECOND_NAME = "second_name";
    public static final String TAG_DATE_OF_BIRTH = "date_of_birth";
    public static final String TAG_GENDER = "gender";
    public static final String TAG_PHONE_NUMBER = "phone_number";
    public static final String TAG_PREGNANCY = "pregnancy";
    public static final String TAG_PREGNANCY_PERIOD = "pregnancy_period";
    public static final String TAG_MENOPAUSE = "menopause";
    public static final String TAG_STATUS = "status";
    public static final String TAG_ANALYSIS_ID = "analysis_uuid";
    public static final String TAG_CREATED_AT = "created_at";


    private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy");

    private static OrderController controller;
    private static Context context;
    private static SqlBaseManager manager;
    private static SharedPreferences sharedPreferences;

    private ArrayList<Order> orderListLocal;
    private ArrayList<Order> orderListServer;

    private boolean isAdd;



    private MedicalTestController medicalTestController;

    private OrderController(){
        orderListLocal = new ArrayList<Order>();
        orderListServer = new ArrayList<Order>();
        manager = new SqlBaseManager(context);
        medicalTestController = MedicalTestController.getController(context);
        sharedPreferences = context.getApplicationContext()
                .getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        getOrderList();
    }

    public static OrderController getController(Context currContext){
        if(controller == null){
            context = currContext;
            controller = new OrderController();
        }
        return controller;
    }

    public boolean isAdd(){ return isAdd;}

    public ArrayList<Order> getOrderList(){
        orderListLocal.clear();
        ArrayList<Order> orderArrayList = new ArrayList<Order>();
        SQLiteDatabase base = manager.getReadableDatabase();
        String selection = TAG_DOCTOR_ID+" = ?";
        String[] selectionArgs = new String[] { UserProcessedController.getController(context).getUUID() };
        Cursor cursor = base.query(HEADER_REQUESTS,null,selection,selectionArgs,null,null,TAG_CREATED_AT+" DESC");

        if(cursor.moveToFirst()){
            int uuid = cursor.getColumnIndex(TAG_UUID);
            int lastName = cursor.getColumnIndex(TAG_LAST_NAME);
            int firstName = cursor.getColumnIndex(TAG_FIRST_NAME);
            int secondName = cursor.getColumnIndex(TAG_SECOND_NAME);
            int dateOfBirth = cursor.getColumnIndex(TAG_DATE_OF_BIRTH);
            int gender = cursor.getColumnIndex(TAG_GENDER);
            int phoneNumber = cursor.getColumnIndex(TAG_PHONE_NUMBER);
            int pregnancy = cursor.getColumnIndex(TAG_PREGNANCY);
            int pregnancyPeriod = cursor.getColumnIndex(TAG_PREGNANCY_PERIOD);
            int menopause = cursor.getColumnIndex(TAG_MENOPAUSE);
            int status = cursor.getColumnIndex(TAG_STATUS);
            int analysisId = cursor.getColumnIndex(TAG_ANALYSIS_ID);
            int createdAT = cursor.getColumnIndex(TAG_CREATED_AT);
            do{
                Customer customer = new Customer();
                customer.setName(cursor.getString(firstName));
                customer.setSurname(cursor.getString(lastName));
                customer.setPatronymics(cursor.getString(secondName));
                customer.setGender((cursor.getInt(gender) == 1) ? true : false);
                customer.setPhone(cursor.getString(phoneNumber));
                customer.setBirthday(new Date(cursor.getInt(dateOfBirth) * 1000));

                Order order = new Order();
                order.setOrderUUID(cursor.getString(uuid));
                order.setCustomer(customer);
                if(customer.isGender()||cursor.getInt(pregnancy) == 1){
                    order.setPregnancy(Order.isHave.No,0);
                }else{
                    order.setPregnancy(
                            ((cursor.getInt(pregnancy) == 2)? Order.isHave.Yes: Order.isHave.Maybe),
                            cursor.getInt(pregnancyPeriod));
                }
                order.setPause(((cursor.getString(menopause).equals("true"))? true: false));
                order.setDateCreate(new Date(cursor.getLong(createdAT) * 1000));
                Log.e("Tests",cursor.getString(analysisId));
                String[] arrayTest = cursor.getString(analysisId).split(Pattern.quote("|"));
                ArrayList<MedicalTest> medicalTests = new ArrayList<MedicalTest>();
                for(int i = 0;i<arrayTest.length;i++){
                    String test = arrayTest[i];//.substring(0,arrayTest[i].length()-1)
                    medicalTests.add(medicalTestController.getTest(test));
                }
                order.setTestsList(medicalTests);
                order.setStatus(cursor.getInt(status));
                orderArrayList.add(order);
            }while (cursor.moveToNext());
        }
        orderArrayList.addAll(orderListServer);
        isAdd = false;
        return orderArrayList;
    }

    @Override
    public ArrayList<Object> getSearchResult(String query) {
        ArrayList<Object> result = new ArrayList<Object>();

        for(Order order : getOrderList()){
            if(order.getSearchString().toLowerCase().contains(query.toLowerCase())){
                result.add(order);
            }
        }
        return result;
    }

    public Order getOrderByUUID(String uuid){
        for(Order order : getOrderList()){
            if(order.getOrderUUID().contains(uuid)){
                return order;
            }
        }
        return null;
    }

    public boolean getServerOrder(){
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet();
        String cookie = sharedPreferences.getString(APP_PREFERENCES_COOKIE, "");
        String uri = SERVER_URL+"?doctor_uuid="+UserProcessedController.getController(context).getUUID();
        httpGet.setURI(URI.create(uri));
        httpGet.setHeader(APP_PREFERENCES_COOKIE, cookie);
        httpGet.setHeader(HEADER_NAME_MODIFI,"0");
        httpGet.setHeader(HEADER_NAME_X_COMAND, HEADER_REQUESTS);
        try{
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");

            HttpResponse response = defaultHttpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 200){
                orderListServer.clear();
                String line = EntityUtils.toString(response.getEntity());
                parseFromJson(line);
                return true;
            }
            if(statusCode == 304){
                return false;
            }
        }catch (IOException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
        return false;
    }
    private void parseFromJson(String response){
        try {
            JSONArray jsonArray = new JSONArray(response);
            for(int i = 0; i < jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                parseToObject(jsonObject);
            }
        }catch (JSONException ex){
            Log.e("JSON analize",ex.getLocalizedMessage());
        }
    }
    private void parseToObject(JSONObject jsonObject){
        try {
            Customer customer = new Customer();
            customer.setName(jsonObject.getString(TAG_FIRST_NAME));
            customer.setSurname(jsonObject.getString(TAG_LAST_NAME));
            customer.setPatronymics(jsonObject.getString(TAG_SECOND_NAME));
            customer.setGender((jsonObject.getInt(TAG_GENDER)==1)?true:false);
            customer.setPhone(jsonObject.getString(TAG_PHONE_NUMBER));
            customer.setBirthday(new Date(jsonObject.getLong(TAG_DATE_OF_BIRTH)*1000));

            Order order = new Order();
            order.setOrderUUID(jsonObject.getString(TAG_UUID));
            order.setCustomer(customer);
            order.setDateCreate(new Date());
            if(customer.isGender()||jsonObject.getInt(TAG_PREGNANCY) == 1){
                order.setPregnancy(Order.isHave.No,0);
            }else{
                order.setPregnancy(
                        ((jsonObject.getInt(TAG_PREGNANCY) == 2)? Order.isHave.Yes: Order.isHave.Maybe),
                        jsonObject.getInt(TAG_PREGNANCY_PERIOD));
            }
            order.setPause(((jsonObject.getString(TAG_MENOPAUSE).equals("true"))? true: false));
            order.setDateCreate(new Date(jsonObject.getLong(TAG_CREATED_AT)*1000));
            JSONArray arrayTest = jsonObject.getJSONArray(TAG_ANALYSIS_ID);
            ArrayList<MedicalTest> medicalTests = new ArrayList<MedicalTest>();
            for(int i = 0;i<arrayTest.length();i++){
                medicalTests.add(medicalTestController.getTestByUUID(arrayTest.getString(i)));
            }
            order.setStatus(jsonObject.getInt(TAG_STATUS));
            order.setTestsList(medicalTests);
            orderListServer.add(order);
        }catch (JSONException ex){
            Log.e("JSON",ex.getLocalizedMessage());
        }
    }

    public boolean insertInBD(Order order){
        SQLiteDatabase writableDatabase = manager.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TAG_UUID, order.getOrderUUID());
        contentValues.put(TAG_DOCTOR_ID, UserProcessedController.getController(context).getUUID());
        contentValues.put(TAG_CREATED_AT, (new Date().getTime())/1000);
        contentValues.put(TAG_LAST_NAME, order.getCustomer().getSurname());
        contentValues.put(TAG_FIRST_NAME, order.getCustomer().getName());
        contentValues.put(TAG_SECOND_NAME, order.getCustomer().getPatronymics());
        contentValues.put(TAG_DATE_OF_BIRTH, order.getCustomer().getBirthday().getTime()/1000);
        contentValues.put(TAG_GENDER, (order.getCustomer().isGender())?1:2);
        contentValues.put(TAG_PHONE_NUMBER, order.getCustomer().getPhone());
        if(order.getCustomer().isGender()||order.getPregnancy().getChildbearing() == Order.isHave.No){
            contentValues.put(TAG_PREGNANCY, 1);
            contentValues.put(TAG_PREGNANCY_PERIOD, "0");
        }else{
            contentValues.put(TAG_PREGNANCY, (order.getPregnancy().getChildbearing() == Order.isHave.Yes)?2:3);
            contentValues.put(TAG_PREGNANCY_PERIOD, order.getPregnancy().getPossibleDate());
        }
        contentValues.put(TAG_MENOPAUSE, (order.isPause())?"true":"false");
        contentValues.put(TAG_STATUS, order.getStatus(true));
        contentValues.put(TAG_ANALYSIS_ID, order.getTestUUID());
        contentValues.put(TAG_CREATED_AT, order.getDateCreateUnixTime());
        long id = writableDatabase.insert(HEADER_REQUESTS, null, contentValues);
        isAdd = true;
        return true;
    }

    public void sendOrdersFromDB(){
        ArrayList<JSONObject> objects = prepareJson();
        if(objects!=null&&objects.size()>0){
            for (JSONObject object:objects){
                if(sendOrder(object)){
                    try {
                        manager.deleteOrder(object.getString(TAG_UUID));
                    }catch (JSONException ex){
                        Log.e("JSON",ex.getLocalizedMessage());
                    }
                }
            }
        }
    }

    public boolean sendOrder(JSONObject holder){
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();

        HttpPost httpPost = new HttpPost(SERVER_URL);

        String cookie = sharedPreferences.getString(APP_PREFERENCES_COOKIE, "");
        httpPost.setHeader(APP_PREFERENCES_COOKIE, cookie);
        httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_REQUESTS);
        try{
            StringEntity se = new StringEntity(holder.toString(),"UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json; charset=utf-8");


            HttpResponse response = defaultHttpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 204){
                manager.deleteOrder(holder.getString("uuid"));
                return true;
            }
            if(statusCode == 400){
                String responseBody = EntityUtils.toString(response.getEntity());
                JSONObject object = new JSONObject(responseBody);
                int code = object.getInt("error_code");
                Log.e(""+statusCode,""+code);
                return false;
            }

        }catch (UnsupportedEncodingException ex){
            Log.e("ENCODING",ex.getLocalizedMessage());
        }catch (IOException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
        catch (JSONException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }

        return false;
    }
    public ArrayList<JSONObject> prepareJson(){

        ArrayList<JSONObject> jsonObjects = new ArrayList<JSONObject>();

        SQLiteDatabase base = manager.getReadableDatabase();
        String selection = TAG_DOCTOR_ID+" = ?";
        String[] selectionArgs = new String[] { UserProcessedController.getController(context).getUUID() };
        Cursor cursor = base.query(HEADER_REQUESTS,null,selection,selectionArgs,null,null,TAG_CREATED_AT+" DESC");

        Log.e("cursor","size "+cursor.getCount());
        if(cursor.moveToFirst()){
            int uuid = cursor.getColumnIndex(TAG_UUID);
            int lastName = cursor.getColumnIndex(TAG_LAST_NAME);
            int firstName = cursor.getColumnIndex(TAG_FIRST_NAME);
            int secondName = cursor.getColumnIndex(TAG_SECOND_NAME);
            int dateOfBirth = cursor.getColumnIndex(TAG_DATE_OF_BIRTH);
            int gender = cursor.getColumnIndex(TAG_GENDER);
            int phoneNumber = cursor.getColumnIndex(TAG_PHONE_NUMBER);
            int pregnancy = cursor.getColumnIndex(TAG_PREGNANCY);
            int pregnancyPeriod = cursor.getColumnIndex(TAG_PREGNANCY_PERIOD);
            int menopause = cursor.getColumnIndex(TAG_MENOPAUSE);
            int status = cursor.getColumnIndex(TAG_STATUS);
            int createdAt = cursor.getColumnIndex(TAG_CREATED_AT);
            int analysisId = cursor.getColumnIndex(TAG_ANALYSIS_ID);
            do{
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(TAG_UUID,cursor.getString(uuid));
                    jsonObject.put(TAG_DOCTOR_UUID,UserProcessedController.getController(context).getUUID());
                    jsonObject.put(TAG_LAST_NAME,cursor.getString(lastName));
                    jsonObject.put(TAG_FIRST_NAME,cursor.getString(firstName));
                    jsonObject.put(TAG_SECOND_NAME,cursor.getString(secondName));
                    jsonObject.put(TAG_DATE_OF_BIRTH,cursor.getLong(dateOfBirth));
                    jsonObject.put(TAG_GENDER,cursor.getInt(gender));
                    jsonObject.put(TAG_PHONE_NUMBER,cursor.getString(phoneNumber));
                    jsonObject.put(TAG_PREGNANCY,cursor.getInt(pregnancy));
                    jsonObject.put(TAG_PREGNANCY_PERIOD,cursor.getLong(pregnancyPeriod));
                    jsonObject.put(TAG_MENOPAUSE,(cursor.getString(menopause).equals("true"))?true:false);
                    jsonObject.put(TAG_STATUS,cursor.getInt(status));
                    String[] test = cursor.getString(analysisId).split(Pattern.quote("|"));
                    JSONArray testJson = new JSONArray();
                    for (int i = 0;i<test.length;i++){
                        MedicalTest medicalTest = MedicalTestController.getController(context).getTest(test[i]);
                        testJson.put(medicalTest.getUUID());
                    }
                    jsonObject.put(TAG_ANALYSIS_ID,testJson);
                    jsonObject.put(TAG_CREATED_AT,cursor.getInt(createdAt));
                    jsonObjects.add(jsonObject);
                }catch (Exception ex){
                    Log.e("JSON",ex.getLocalizedMessage());
                    return null;
                }
            }while (cursor.moveToNext());
            return jsonObjects;
        }else{
            Log.e("List_Orders","No data");
            return null;
        }
    }

}
