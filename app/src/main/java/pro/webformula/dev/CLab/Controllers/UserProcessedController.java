package pro.webformula.dev.CLab.Controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.jar.JarException;

import pro.webformula.dev.CLab.POJO.ValidationModel;
import pro.webformula.dev.CLab.UI.ParentActivity;
import pro.webformula.dev.CLab.UI.SettingsFragment;

/**
 * Created by dev on 02.03.2015.
 */
public class UserProcessedController {

    private static final String APP_PREFERENCES = "CityLab";
    public static final String APP_PREFERENCES_LOGIN = "Logined";
    public static final String APP_PREFERENCES_USER_MAIL = "UserMail";
    public static final String APP_PREFERENCES_COOKIE = "Cookie";
    public static final String APP_PREFERENCES_USER_ID = "uuid";
    public static final String APP_PREFERENCES_MAIL = "Mail";
    public static final String APP_PREFERENCES_FIRST = "FirstTime";
    public static final String SERVER_URL ="http://195.128.126.168:84/api/core.ashx";
    public static final String HEADER_NAME_X_COMAND = "X-Command";
    public static final String HEADER_TYPE_REG = "Registration";
    public static final String HEADER_TYPE_LOGIN = "Login";
    public static final String HEADER_TYPE_TOKEN = "SetToken";
    public static final String HEADER_TYPE_VERIFY = "Verify";
    public static final String HEADER_COOKIE = "Set-Cookie";
    public static final String PROPERTY_REG_ID = "registration_id";

    private boolean isNewUser;

    private static UserProcessedController controller;
    private static Context context;

    private boolean sucessConnection;
    private String[] regArrayJSON;
    private ValidationModel model;


    private SharedPreferences sharedPreferences;

    private UserProcessedController(){
        sharedPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static UserProcessedController getController(Context currContext){
        if(controller == null){
            context = currContext;
            controller = new UserProcessedController();
        }
        return controller;
    }

    public void setLogin(boolean isLogined){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(APP_PREFERENCES_LOGIN, isLogined);
        if(!isLogined){
            editor.putString(APP_PREFERENCES_USER_ID, "");
            editor.putString(PROPERTY_REG_ID,"");
        }
        editor.commit();
    }

    public boolean checkLogin(){
        return sharedPreferences.getBoolean(APP_PREFERENCES_LOGIN, false);
    }
    public boolean checkFirstTime(){
        return sharedPreferences.getBoolean(APP_PREFERENCES_FIRST, false);
    }
    public void setFirstTime(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(APP_PREFERENCES_FIRST, true);
        editor.commit();
    }

    public void setMail(String mail){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(APP_PREFERENCES_MAIL, mail);
        editor.commit();
    }

    public String getMail(){
        return sharedPreferences.getString(APP_PREFERENCES_MAIL, "");
    }

    public void setUUID(String uuid){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(APP_PREFERENCES_USER_ID, uuid);
        editor.commit();
    }

    public void setSettingsPreference(boolean action, boolean holidays){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SettingsFragment.HEADER_TYPE_ACTIONS, action);
        editor.putBoolean(SettingsFragment.HEADER_TYPE_HOLIDAY, holidays);
        editor.commit();
    }

    public void setUserMail(String mail){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(APP_PREFERENCES_USER_MAIL, mail);
        editor.commit();
    }

    public String getUUID(){
        return sharedPreferences.getString(APP_PREFERENCES_USER_ID, "");
    }

    public int sendValidationData(ValidationModel model,String[] jsonFields, boolean isNewUser){
        JSONObject json;
        this.isNewUser = isNewUser;
        int res = -1;
        try {
            if(isNewUser){
                json = ValidationModel.getJsonObjectForRegistration(model, jsonFields);
            }else{
                json = ValidationModel.getJsonObjectForLogin(model, jsonFields);
            }
            Log.e("JSON", json.toString());
            /*HTTPTask task = new HTTPTask();
            try {
                task.execute(json);
                res = task.get();
                Log.e("ADDAPT","code "+res);
            }catch (Exception ex){
                Log.e("TASK",ex.getLocalizedMessage());
            }*/
            res = executeServer(json);
        }catch (JSONException ex){
            Log.e("JSON", ex.getLocalizedMessage());
        }
        return res;
    }

    private class HTTPTask extends AsyncTask<JSONObject,Void,Integer> {
        @Override
        protected Integer doInBackground(JSONObject... params) {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(SERVER_URL);
            if(isNewUser){
                httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_REG);
            }else{
                httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_LOGIN);
            }

            JSONObject holder = params[0];

            try{
                StringEntity se = new StringEntity(holder.toString(),"UTF-8");
                httpPost.setEntity(se);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json;charset=utf-8");

                HttpResponse response = httpclient.execute(httpPost);
                int statusCode = response.getStatusLine().getStatusCode();
                if(statusCode == 200){
                    if(!isNewUser){
                        Header[] cookie = response.getAllHeaders();
                        for(int i = 0;i<cookie.length;i++){
                            if(cookie[i].getName().equals(HEADER_COOKIE)){
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(APP_PREFERENCES_COOKIE,cookie[i].getValue());
                                editor.commit();
                                break;
                            }
                        }
                    }
                    String responseBody = EntityUtils.toString(response.getEntity());
                    Log.e("RES",responseBody);
                    return 0;
                }
                if(statusCode == 400){
                    String responseBody = EntityUtils.toString(response.getEntity());
                    JSONObject object = new JSONObject(responseBody);
                    int code = object.getInt("error_code");
                    Log.e("CODE",""+code);
                    return code;
                }
                Log.e("StatusCODE",""+statusCode);

            }catch (UnsupportedEncodingException ex){
                Log.e("ENCODING",ex.getLocalizedMessage());
            }catch (IOException ex){
                Log.e("IO",ex.getLocalizedMessage());
            }
            catch (JSONException ex){
                Log.e("IO",ex.getLocalizedMessage());
            }

            return -1;
        }
    }
    public int executeServer(JSONObject holder){
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();

        HttpPost httpPost = new HttpPost(SERVER_URL);
        if(isNewUser){
            httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_REG);
        }else{
            httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_LOGIN);
        }

        try{
            StringEntity se = new StringEntity(holder.toString(),"UTF-8");

            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse response = defaultHttpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 200){
                String responseBody = EntityUtils.toString(response.getEntity());
                if(!isNewUser){
                    Header[] cookie = response.getAllHeaders();
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    for(int i = 0;i<cookie.length;i++){
                        if(cookie[i].getName().equals(HEADER_COOKIE)){
                            editor.putString(APP_PREFERENCES_COOKIE,cookie[i].getValue());
                            editor.commit();
                            break;
                        }
                    }
                    JSONObject object = new JSONObject(responseBody);
                    setUUID(object.getString(APP_PREFERENCES_USER_ID));
                    boolean actions = (object.getString("actions_enable").equals("true")?true:false);
                    boolean holidays = (object.getString("holidays_enable").equals("true")?true:false);
                    setUserMail(holder.getString("login"));
                    setSettingsPreference(actions,holidays);
                    //editor.putString(APP_PREFERENCES_USER_ID,object.getString(APP_PREFERENCES_USER_ID));
                    //editor.commit();
                    Log.e("UUID", sharedPreferences.getString(APP_PREFERENCES_USER_ID, ""));
                }
                return 0;
            }
            if(statusCode == 400){
                String responseBody = EntityUtils.toString(response.getEntity());
                JSONObject object = new JSONObject(responseBody);
                int code = object.getInt("error_code");
                Log.e("CODE "+statusCode,""+code);
                return code;
            }

        }catch (UnsupportedEncodingException ex){
            Log.e("ENCODING",ex.getLocalizedMessage());
        }catch (IOException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
        catch (JSONException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }

        return -1;
    }


    public int validationMailOnServer(String code){
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(SERVER_URL);
        httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_VERIFY);
        JSONObject object = new JSONObject();
        try{

            object.put("login",getMail());
            object.put("code",code);
            StringEntity se = new StringEntity(object.toString(),"UTF-8");

            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse response = defaultHttpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 200){
                String responseBody = EntityUtils.toString(response.getEntity());
                Log.e("RES",responseBody);
                return 0;
            }
            if(statusCode == 400){
                String responseBody = EntityUtils.toString(response.getEntity());
                JSONObject objectRes = new JSONObject(responseBody);
                int codeRes = objectRes.getInt("error_code");
                Log.e("CODE",""+codeRes);
                return codeRes;
            }
            Log.e("StatusCODE",""+statusCode);

        }catch (UnsupportedEncodingException ex){
            Log.e("ENCODING",ex.getLocalizedMessage());
        }catch (IOException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
        catch (JSONException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }

        return -1;
    }

    public void sendToken(String token){

        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(SERVER_URL);
        String cookie = sharedPreferences.getString(APP_PREFERENCES_COOKIE, "");
        httpPost.setHeader(APP_PREFERENCES_COOKIE,cookie);
        httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_TOKEN);
        JSONObject object = new JSONObject();
        try{

            object.put("token",token);
            StringEntity se = new StringEntity(object.toString(),"UTF-8");

            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse response = defaultHttpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 200){
                Log.e("RES", "TokenSend");
            }
            if(statusCode == 400){
                String responseBody = EntityUtils.toString(response.getEntity());
                JSONObject objectRes = new JSONObject(responseBody);
                int codeRes = objectRes.getInt("error_code");
                Log.e("CODE",""+codeRes);
            }
            Log.e("StatusCODE",""+statusCode);

        }catch (UnsupportedEncodingException ex){
            Log.e("ENCODING",ex.getLocalizedMessage());
        }catch (IOException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
        catch (JSONException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return context.getSharedPreferences(ParentActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

}
