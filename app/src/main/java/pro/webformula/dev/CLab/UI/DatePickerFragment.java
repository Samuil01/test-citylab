package pro.webformula.dev.CLab.UI;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;
import java.util.regex.Pattern;

import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 20.02.2015.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private EditText editTextDate;
    private int year;
    private int month;
    private int day;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();

        if(editTextDate.getText().toString().equals("")){
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }else{
            String[] date = editTextDate.getText().toString().split(Pattern.quote("."));
            year = Integer.parseInt(date[2]);
            month = Integer.parseInt(date[1])-1;
            day = Integer.parseInt(date[0]);
        }

        Dialog picker = new DatePickerDialog(getActivity(), this,
                year, month, day);
        picker.setTitle(getResources().getString(R.string.chose));

        return picker;
    }
    @Override
    public void onStart() {
        super.onStart();
        Button nButton =  ((AlertDialog) getDialog())
                .getButton(DialogInterface.BUTTON_POSITIVE);
        nButton.setText(getResources().getString(R.string.ready));

    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year,
                          int month, int day) {
        editTextDate.setText(day + "." + (month+1) + "." + year);
    }

    public void setEditTextElement(EditText editText){
        editTextDate = editText;
    }
}