package pro.webformula.dev.CLab.UI;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import pro.webformula.dev.CLab.POJO.News;
import pro.webformula.dev.CLab.Controllers.NewsController;
import pro.webformula.dev.CLab.R;
import pro.webformula.dev.CLab.SqlBaseManager;

/**
 * Created by dev on 17.02.2015.
 */
public class NewsActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        getActionBar().hide();
        String uuid = getIntent().getStringExtra(NewsController.EXTRA_NEWS);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(230, 230)
                .build();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.news_loader)
                .showImageOnLoading(R.drawable.news_loader)
                .build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        News news = new SqlBaseManager(getApplicationContext()).getNewsFromDB(uuid);
        TextView textViewTitle = (TextView)findViewById(R.id.news_item_title);
        TextView textViewTime = (TextView)findViewById(R.id.news_item_time);
        TextView textViewDesc = (TextView)findViewById(R.id.news_item_description);
        ImageView imageViewNews = (ImageView)findViewById(R.id.news_item_img);
        ImageView imageViewBack = (ImageView)findViewById(R.id.item_test_list_back);

        textViewTitle.setText(news.getTitle());
        textViewTime.setText(news.getDate());
        textViewDesc.setText(Html.fromHtml(news.getDescription()));
        imageLoader.displayImage(news.getImg(),imageViewNews,options);
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
