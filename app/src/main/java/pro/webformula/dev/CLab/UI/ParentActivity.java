package pro.webformula.dev.CLab.UI;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import pro.webformula.dev.CLab.Controllers.FilialAgencyController;
import pro.webformula.dev.CLab.Controllers.MedicalTestController;
import pro.webformula.dev.CLab.Controllers.NewsController;
import pro.webformula.dev.CLab.Controllers.OrderController;
import pro.webformula.dev.CLab.Controllers.UserProcessedController;
import pro.webformula.dev.CLab.R;

public class ParentActivity extends Activity implements ActionBar.TabListener {

    private static final String APP_PREFERENCES = "CityLab";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    NewsFragment newsFragment;
    OrdersFragment ordersFragment;
    MapsFragment mapsFragment;
    SettingsFragment settingsFragment;
    ActionBar bar;
    FragmentTransaction fTrans;
    Menu appMenu;
    boolean isAction;

    private ProgressDialog progressDialog;

    String SENDER_ID = "352092409956";
    String regid;
    GoogleCloudMessaging gcm;
    Context context;

    int currTab = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);
        context = getApplicationContext();

        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);
            if (regid.isEmpty()) {
                registerInBackground();
            }
        }

        newsFragment = new NewsFragment();
        ordersFragment = new OrdersFragment();
        mapsFragment = new MapsFragment();
        settingsFragment = new SettingsFragment();


        Intent intent = getIntent();
        if(intent!= null){
            isAction = intent.getBooleanExtra("ACTION",false);
        }

        bar = getActionBar();
        bar.setIcon(R.drawable.overflow);
        tabCreate(bar);
        if(!isAction){
            bar.setSelectedNavigationItem(1);
        }
        updateBD();
    }



    public void tabCreate(ActionBar bar){

        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        Tab tab = bar.newTab();
        tab.setText(R.string.news);
        tab.setTabListener(this);
        bar.addTab(tab);

        tab = bar.newTab();
        tab.setText(R.string.orders);
        tab.setTabListener(this);
        bar.addTab(tab);

        tab = bar.newTab();
        tab.setText(R.string.map);
        tab.setTabListener(this);
        bar.addTab(tab);

        tab = bar.newTab();
        tab.setText(R.string.settings);
        tab.setTabListener(this);
        bar.addTab(tab);

    }

    public void updateBD(){
        new UpdateTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        appMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {

        fTrans = getFragmentManager().beginTransaction();
        currTab = tab.getPosition();
        switch (tab.getPosition()) {
            case 0:
                Bundle bundle = new Bundle();
                if(isAction){
                    bundle.putBoolean("Action",true);
                    newsFragment.setArguments(bundle);
                }
                fTrans.add(R.id.fragment_area, newsFragment);
                bar.setTitle(R.string.news);
                break;
            case 1:
                fTrans.add(R.id.fragment_area, ordersFragment);
                bar.setTitle(R.string.orders);
                break;
            case 2:
                fTrans.add(R.id.fragment_area, mapsFragment);
                bar.setTitle(R.string.map);
                break;
            case 3:
                fTrans.add(R.id.fragment_area, settingsFragment);
                bar.setTitle(R.string.settings);
                break;
            default:
                break;
        }
        fTrans.commit();
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
        fTrans = getFragmentManager().beginTransaction();
        currTab = tab.getPosition();
        switch (currTab) {
            case 0:
                fTrans.remove(newsFragment);
                break;
            case 1:
                fTrans.remove(ordersFragment);
                break;
            case 2:
                fTrans.remove(mapsFragment);
                break;
            case 3:
                fTrans.remove(settingsFragment);
                break;
            default:
                break;
        }
        fTrans.commit();
    }

    private class UpdateTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            progressDialog = new ProgressDialog(ParentActivity.this);
            progressDialog.setTitle("Пожалуйста, подождите");
            progressDialog.setMessage("Синхронизация...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            MedicalTestController medicalTestController = MedicalTestController.getController(getApplicationContext());
            if(medicalTestController.getServerTests()){
                Log.e("update analyze","done");
            }else{
                Log.e("update analyze","note update");
            }
            OrderController orderController = OrderController.getController(getApplicationContext());
            if(orderController.getServerOrder()){
                Log.e("update order","done");
            }else{
                Log.e("update order","note update");
            }
            NewsController newsController = NewsController.getController(getApplicationContext());
            if(newsController.getServerNews(true)){
                Log.e("update news","done");
            }else{
                Log.e("update news","note update");
            }
            FilialAgencyController controller = FilialAgencyController.getController(getApplicationContext());
            if(controller.getServerFilial()){
                Log.e("update filial","done");
            }else{
                Log.e("update filial","note update");
            }




            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(!isAction){
                ordersFragment.refresh();
            }
            progressDialog.dismiss();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i("GCM", "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i("GCM", "App version changed.");
            return "";
        }
        return registrationId;
    }
    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(APP_PREFERENCES,
                Context.MODE_PRIVATE);
    }
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void registerInBackground() {
        new AsyncTask<Void,Void,String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    UserProcessedController.getController(context).sendToken(regid);
                    Log.e("GSM_ID","id "+regid);
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.e("GSM_MSG",msg);
            }
        }.execute(null, null, null);
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i("GCM", "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            Log.e("Push","false");
            return;
        }
        boolean action = data.getBooleanExtra("ACTION", false);
        if(action){
            Toast.makeText(getApplicationContext(),"ACTIONS",Toast.LENGTH_LONG).show();
            Log.e("Push","yes");
        }else {
            Toast.makeText(getApplicationContext(),"Not ACTIONS",Toast.LENGTH_LONG).show();
            Log.e("Push","no");
        }
    }
}
