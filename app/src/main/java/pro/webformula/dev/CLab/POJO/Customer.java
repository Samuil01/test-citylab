package pro.webformula.dev.CLab.POJO;

import java.util.Date;
import java.util.UUID;

/**
 * Created by dev on 13.02.2015.
 */
public class Customer {

    private UUID id;
    private String name;
    private String surname;
    private String patronymics;
    private Date birthday;
    private boolean gender;
    private String phone;
    private String searchString;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        searchString +=name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
        searchString+=surname;
    }

    public String getPatronymics() {
        return patronymics;
    }

    public void setPatronymics(String patronymics) {
        this.patronymics = patronymics;
        searchString+=patronymics;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSearchString(){return searchString;}

    public static Customer getFakeCust(int i){
        Customer c = new Customer();
        c.setName("Some");
        c.setSurname("One one of as");
        c.setPatronymics(""+i);
        return c;
    }

    public String getCustomerTitle(){
        return surname+" "+name.substring(0,1)+"."+patronymics.substring(0,1);
    }
}
