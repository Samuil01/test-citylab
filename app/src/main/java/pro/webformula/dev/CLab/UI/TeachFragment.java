package pro.webformula.dev.CLab.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 25.03.2015.
 */
public class TeachFragment  extends Fragment {

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";

    int pageNumber;
    String[] text;

    static TeachFragment newInstance(int page) {
        TeachFragment pageFragment = new TeachFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
        text = getResources().getStringArray(R.array.teach_text);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teach_slide, null);

        TextView tvPage = (TextView) view.findViewById(R.id.teach_slide_text);
        ImageView imageViewNext = (ImageView)view.findViewById(R.id.teach_slide_next);
        ImageView imageViewSlide = (ImageView)view.findViewById(R.id.teach_slide);

        tvPage.setText(text[pageNumber]);
        imageViewSlide.setImageResource(getResources().getIdentifier("illustration_" + (pageNumber + 1),
                "drawable", getActivity().getApplicationContext().getPackageName()));
        imageViewNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pageNumber == 2){
                    getActivity().finish();
                }else{
                    TeachingSlideActivity.nextSlide();
                }
            }
        });
        return view;
    }
}