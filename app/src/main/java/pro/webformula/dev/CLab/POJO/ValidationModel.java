package pro.webformula.dev.CLab.POJO;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.UUID;

import pro.webformula.dev.CLab.Controllers.FieldValidator;

/**
 * Created by dev on 05.03.2015.
 */
public class ValidationModel{
    HashMap<Integer,String> validationField;

    public ValidationModel(){
        validationField = new HashMap<Integer, String>();
    }

    public void setValidationField(int position, String value){
        validationField.put(position,value);
    }
    public HashMap<Integer,String> getValidationField(){
        return validationField;
    }

    public String getValue(int position){
        return validationField.get(position);
    }

    public boolean isEmptyField(int position){
        return validationField.get(position).equals("");
    }
    public boolean keyContain(int position){
        return validationField.containsKey(position);
    }
    public boolean valueContain(String value){
        return validationField.containsValue(value);
    }
    public int getPosition(String value){
        for(Integer position:validationField.keySet()){
            if(validationField.get(position).equals(value)){
                return position;
            }
        }
        return -1;
    }

    public static JSONObject getJsonObjectForRegistration(ValidationModel model,String[] JSONkey) throws JSONException {
        JSONObject holder = new JSONObject();
        for(int idKey = 0;idKey<JSONkey.length-1;idKey++){
            switch (idKey) {
                case 5:
                    holder.put(JSONkey[idKey], (FieldValidator.getUnixTime(model.getValue(idKey))/1000));
                    Log.e("JSON", "Set key " + idKey + " key " + JSONkey[idKey] + " value" +(FieldValidator.getUnixTime(model.getValue(idKey))/1000));
                    break;
                case 6:
                    holder.put(JSONkey[idKey], FieldValidator.getGender(model.getValue(idKey)));
                    Log.e("JSON", "Set key " + idKey + " key " + JSONkey[idKey] + " value" + FieldValidator.getGender(model.getValue(idKey)));
                    break;
                default:
                    holder.put(JSONkey[idKey], model.getValue(idKey));
                    Log.e("JSON", "Set key " + idKey + " key " + JSONkey[idKey] + " value" + model.getValue(idKey));
                    break;
            }
        }
        holder.put(JSONkey[JSONkey.length-1], UUID.randomUUID());
        Log.e("JSON", "Set key " + (JSONkey.length-1) + " key " + JSONkey[JSONkey.length-1] + " value" + holder.get(JSONkey[JSONkey.length-1]));
        return holder;
    }

    public static JSONObject getJsonObjectForLogin(ValidationModel model,String[] JSONkey) throws JSONException {
        JSONObject holder = new JSONObject();
        for(int idKey = 0;idKey<JSONkey.length;idKey++){
            holder.put(JSONkey[idKey], model.getValue(idKey));
        }
        return holder;
    }
}