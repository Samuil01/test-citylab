package pro.webformula.dev.CLab.UI;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 24.02.2015.
 */
public class SearchActivity extends Activity {

    public static final String SEARCH_TYPE = "type";

    public enum fragmentSwitcher {dummy,search,begin};

    private int searchType;
    private boolean isInput;
    private fragmentSwitcher status;
    private EditText searchEditText;
    private FragmentTransaction transaction;
    private FragmentDummySearch dummySearch;
    private FragmentSearchResult searchResult;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchType = getIntent().getIntExtra(SEARCH_TYPE,0);
        setContentView(R.layout.activity_search);
        dummySearch = new FragmentDummySearch();
        searchResult = new FragmentSearchResult();
        prepareView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void prepareView(){
        getActionBar().hide();

        switchFragment(fragmentSwitcher.begin);

        searchEditText = (EditText)findViewById(R.id.search_text);
        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                switchFragment(fragmentSwitcher.search);
                if(!s.toString().equals("")){
                    searchResult.setSearchText(s.toString());
                    isInput = true;
                }else{
                    isInput = false;
                }
            }
        });


        ImageView backImageView = (ImageView)findViewById(R.id.search_back);
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchType == 2){
                    if(isInput)
                        searchResult.onSelectedTestSend();
                    else
                        finish();

                }else{
                    finish();
                }
            }
        });
        Button clearImageView = (Button)findViewById(R.id.search_clear);
        clearImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!searchEditText.getText().toString().equals("")){
                    searchEditText.setText("");
                    switchFragment(fragmentSwitcher.dummy);
                }

            }
        });
    }

    public String getSearch(){
        return searchEditText.getText().toString();
    }

    public void switchFragment(fragmentSwitcher switcher){
        Bundle args = new Bundle();
        if(status != switcher){
            transaction = getFragmentManager().beginTransaction();
            args.putInt(SEARCH_TYPE, searchType);
            switch (switcher){
                case dummy:
                    transaction.remove(searchResult);
                    dummySearch.setArguments(args);
                    transaction.add(R.id.search_fragment, dummySearch);
                    status = fragmentSwitcher.dummy;
                    break;
                case search:
                    transaction.remove(dummySearch);
                    searchResult.setArguments(args);
                    transaction.add(R.id.search_fragment, searchResult);
                    status=fragmentSwitcher.search;
                    break;
                case begin:
                    dummySearch.setArguments(args);
                    transaction.add(R.id.search_fragment, dummySearch);
                    status=fragmentSwitcher.dummy;
            }
            transaction.commit();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        int id = data.getIntExtra("idResult",0);
    }
}