package pro.webformula.dev.CLab.UI;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 24.03.2015.
 */
public class FeedBackActivity extends Activity{
    public static final String SERVER_URL ="http://195.128.126.168:84/api/core.ashx";
    public static final String HEADER_NAME_X_COMAND = "X-Command";
    public static final String HEADER_TYPE_FEED = "Feedback";
    private static final String APP_PREFERENCES = "CityLab";
    public static final String APP_PREFERENCES_COOKIE = "Cookie";

    private SharedPreferences sharedPreferences;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getActionBar().hide();
        sharedPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        ImageView back  = (ImageView)findViewById(R.id.item_call_back);
        ImageView ok = (ImageView)findViewById(R.id.callback_ok);
        final EditText editText = (EditText)findViewById(R.id.callback_text);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText.getText().toString().equals("")){
                    finish();
                }else{
                    new FeedbackSend().execute(editText.getText().toString());
                    finish();
                }
            }
        });

    }

    public void send(String text){

        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(SERVER_URL);
        String cookie = sharedPreferences.getString(APP_PREFERENCES_COOKIE, "");
        httpPost.setHeader(APP_PREFERENCES_COOKIE,cookie);
        httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_FEED);
        JSONObject object = new JSONObject();
        try{

            object.put("text",text);
            StringEntity se = new StringEntity(object.toString(),"UTF-8");

            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse response = defaultHttpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 200){
                String responseBody = EntityUtils.toString(response.getEntity());
                Log.e("RES", responseBody);
            }
            if(statusCode == 400){
                String responseBody = EntityUtils.toString(response.getEntity());
                JSONObject objectRes = new JSONObject(responseBody);
                int codeRes = objectRes.getInt("error_code");
                Log.e("CODE",""+codeRes);
            }
            Log.e("StatusCODE",""+statusCode);

        }catch (UnsupportedEncodingException ex){
            Log.e("ENCODING",ex.getLocalizedMessage());
        }catch (IOException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
        catch (JSONException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
    }

    public class FeedbackSend extends AsyncTask<String,Void,Void>{

        @Override
        protected Void doInBackground(String... params) {
            send(params[0]);
            return null;
        }
    }
}
