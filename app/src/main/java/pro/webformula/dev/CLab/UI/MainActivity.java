package pro.webformula.dev.CLab.UI;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import pro.webformula.dev.CLab.Controllers.UserProcessedController;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 02.03.2015.
 */
public class MainActivity extends Activity {

    public static final String EXTRA_NEW_USER = "isNew";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        if(!UserProcessedController.getController(getApplicationContext()).checkFirstTime()){
            Intent intent = new Intent(getApplicationContext(),TeachingSlideActivity.class);
            startActivity(intent);
            UserProcessedController.getController(getApplicationContext()).setFirstTime();
        }
        if(UserProcessedController.getController(getApplicationContext()).checkLogin()){
            Intent intent = new Intent(getApplicationContext(),ParentActivity.class);
            startActivity(intent);
            finish();
        }else{
            setContentView(R.layout.activity_main);
            prepareView();
        }
    }

    public void prepareView(){

        TextView loginTextView = (TextView)findViewById(R.id.login);
        TextView registrationTextView = (TextView)findViewById(R.id.registration);
        final Intent intent = new Intent(getApplicationContext(),UserProcessedActivity.class);

        registrationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra(EXTRA_NEW_USER,true);
                startActivity(intent);
            }
        });

        loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra(EXTRA_NEW_USER,false);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(UserProcessedController.getController(getApplicationContext()).checkLogin()){
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(UserProcessedController.getController(getApplicationContext()).checkLogin()){
            finish();
        }
    }

}
