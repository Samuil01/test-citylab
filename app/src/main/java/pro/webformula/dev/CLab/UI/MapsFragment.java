package pro.webformula.dev.CLab.UI;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import pro.webformula.dev.CLab.POJO.FilialAgency;
import pro.webformula.dev.CLab.Controllers.FilialAgencyController;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 11.02.2015.
 */
public class MapsFragment extends Fragment {

    public static final String EXTRA_FILIAL = "id";


    private static final String APP_PREFERENCES = "CityLab";
    private static final String APP_LOCATION_X = "X";
    private static final String APP_LOCATION_Y = "Y";

    private MapView mapView;
    private GoogleMap map;
    private MarkerOptions marker;

    private LocationManager locationManager;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ArrayList<FilialAgency> filialAgencies;

    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        sharedPreferences = getActivity().getApplicationContext()
                .getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        filialAgencies = FilialAgencyController.getController(getActivity().getApplicationContext()).getFilialAgencies();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_maps, container, false);

        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            Log.e("Address Map", "Could not initialize google play", e);
        }

        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()) )
        {
            case ConnectionResult.SUCCESS:
                mapView = (MapView) v.findViewById(R.id.map);
                mapView.onCreate(savedInstanceState);
                if(mapView!=null)
                {
                    map = mapView.getMap();
                    map.getUiSettings().setMyLocationButtonEnabled(false);
                    map.setMyLocationEnabled(true);
                    setCamera(getCoordinate());
                }
                map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        int id = Integer.parseInt(marker.getId().substring(1));
                        Intent intent = new Intent(getActivity().getApplicationContext(),FilialAgencyActivity.class);
                        intent.putExtra(EXTRA_FILIAL,id);
                        startActivity(intent);
                    }
                });
                break;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(getActivity(), "SERVICE MISSING", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(getActivity(), "UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                break;
            default: Toast.makeText(getActivity(), GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()), Toast.LENGTH_SHORT).show();
        }

        getMarkers();

        return v;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        menu.add(0, R.menu.menu_main, 1, R.string.news)
                .setIcon(R.drawable.overflow_2)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = new Intent(getActivity().getApplicationContext(),FilialAgencyList.class);
                        startActivity(intent);
                        return false;
                    }
                })
                .setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);

        menu.add(0, R.menu.menu_main, 1, R.string.news)
                .setIcon(R.drawable.search)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = new Intent(getActivity().getApplicationContext(),SearchActivity.class);
                        intent.putExtra(SearchActivity.SEARCH_TYPE,1);
                        startActivityForResult(intent, 1);
                        return false;
                    }
                })
                .setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        saveLastPosition();
        mapView.onResume();
        super.onResume();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        int ids = data.getIntExtra("idResult",0);
        FilialAgency filialAgency = FilialAgencyController.getController(getActivity()).getFilialAgencies().get(ids);
        LatLng latLng = new LatLng(filialAgency.getCoordinateX(),filialAgency.getCoordinateY());
        setCamera(latLng);
    }

    public void getMarkers(){
        for(FilialAgency filialAgency : filialAgencies){
            marker = new MarkerOptions();
            marker.position(new LatLng(filialAgency.getCoordinateX(), filialAgency.getCoordinateY()));
            //marker.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_compass));
            marker.title(filialAgency.getAddress());
            marker.snippet(filialAgency.getPhone());
            map.addMarker(marker);
        }
    }

    public void setCamera(LatLng latLng){
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        map.animateCamera(cameraUpdate);
    }

    public LatLng getCoordinate(){
        if(locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)){
            Location location = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            if(location != null){
                return new LatLng(location.getLatitude(),location.getLongitude());
            }
        }
        if(locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER)){
            Location location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
            if(location != null){
                return new LatLng(location.getLatitude(),location.getLongitude());
            }
        }
        if((sharedPreferences.getFloat(APP_LOCATION_X,0) != 0) && (sharedPreferences.getFloat(APP_LOCATION_Y,0) != 0)){
            Toast.makeText(getActivity().getApplicationContext(),"From preference", Toast.LENGTH_SHORT).show();
            return new LatLng(sharedPreferences.getFloat(APP_LOCATION_X, 0),sharedPreferences.getFloat(APP_LOCATION_Y,0));
        }
        return new LatLng(45.018, 38.96);
    }
    public void saveLastPosition(){
        editor.putFloat(APP_LOCATION_X,((float)getCoordinate().latitude));
        editor.putFloat(APP_LOCATION_Y,((float)getCoordinate().longitude));
        editor.commit();
    }
}