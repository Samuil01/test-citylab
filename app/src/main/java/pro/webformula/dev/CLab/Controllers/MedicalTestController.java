package pro.webformula.dev.CLab.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import pro.webformula.dev.CLab.POJO.FilialAgency;
import pro.webformula.dev.CLab.POJO.MedicalTest;
import pro.webformula.dev.CLab.SqlBaseManager;

/**
 * Created by dev on 18.02.2015.
 */
public class MedicalTestController implements Controllers{

    private static final String APP_PREFERENCES = "CityLab";
    public static final String APP_PREFERENCES_COOKIE = "Cookie";

    public static final String SERVER_URL ="http://195.128.126.168:84/api/core.ashx";
    public static final String HEADER_NAME_MODIFI = "If-Modified-Since";
    public static final String HEADER_NAME_X_COMAND = "X-Command";
    public static final String HEADER_ANALYZES = "Analyzes";

    public static final String TAG_UUID = "uuid";
    public static final String TAG_NAME = "name";
    public static final String TAG_PRICE = "price";
    public static final String TAG_DURATION = "duration";
    public static final String TAG_ICON = "icon";
    public static final String TAG_NUMBER = "number";
    public static final String TAG_BRIEF_DESC = "brief_desc";
    public static final String TAG_FULL_DESC = "full_desc";
    public static final String TAG_ADDITIONAL_DESC = "additionally_desc";
    public static final String TAG_CATEGORY = "category";
    public static final String TAG_DELETED = "deleted";
    public static final String TAG_UPDATED_AT = "updated_at";

    private static MedicalTestController controller;
    private static Context context;
    private ArrayList<MedicalTest> tests;
    private ArrayList<String> group;

    private static SqlBaseManager manager;
    private static SharedPreferences sharedPreferences;

    private MedicalTestController(){
        tests = new ArrayList<MedicalTest>();
        group = new ArrayList<String>();
        manager = new SqlBaseManager(context);
        sharedPreferences = context.getApplicationContext()
                .getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        /*if(manager.getNewerDate(HEADER_ANALYZES,TAG_UPDATED_AT)!=0){
            initAnalyzes();
        }*/

        initAnalyzes();
    }
    private void initAnalyzes(){
        if(tests.size() != 0)
            return;
        SQLiteDatabase base = manager.getReadableDatabase();
        String selection = TAG_DELETED+" = ?";
        String[] selectionArgs = new String[] { "false" };
        Cursor cursor = base.query(HEADER_ANALYZES,null,selection,selectionArgs,null,null,null);
        if(cursor.moveToFirst()){
            tests.clear();
            int id = 0;
            int uuid = cursor.getColumnIndex(TAG_UUID);
            int name = cursor.getColumnIndex(TAG_NAME);
            int price = cursor.getColumnIndex(TAG_PRICE);
            int duration = cursor.getColumnIndex(TAG_DURATION);
            int number = cursor.getColumnIndex(TAG_NUMBER);
            int icon = cursor.getColumnIndex(TAG_ICON);
            int brief_desc = cursor.getColumnIndex(TAG_BRIEF_DESC);
            int full_desc = cursor.getColumnIndex(TAG_FULL_DESC);
            int additional_desc  = cursor.getColumnIndex(TAG_ADDITIONAL_DESC);
            int category = cursor.getColumnIndex(TAG_CATEGORY);
            do{
                MedicalTest medicalTest = new MedicalTest();
                medicalTest.setId(id);
                medicalTest.setUUID(cursor.getString(uuid));
                medicalTest.setName(cursor.getString(name));
                medicalTest.setNumber(cursor.getString(number));
                medicalTest.setPrice(cursor.getDouble(price));
                medicalTest.setTime(cursor.getInt(duration));
                medicalTest.setType(cursor.getString(category));
                medicalTest.setImg("icon_"+cursor.getString(icon));
                medicalTest.setDesk(cursor.getString(brief_desc));
                /*filialAgency.setMapAddress(getAddressFromLatLng(filialAgency.getCoordinateX(),
                        filialAgency.getCoordinateY()));*/
                //new ShadowTask().execute(filialAgency);
                tests.add(medicalTest);
                id++;
            }while (cursor.moveToNext());
        }
        initCategory();
    }
    private void initCategory(){
        for (MedicalTest test : tests){
            if(!group.contains(test.getType())){
                group.add(test.getType());
            }
        }
    }

    public static MedicalTestController getController(Context currContext){
        if(controller == null){
            context = currContext;
            controller = new MedicalTestController();
        }
        return controller;
    }

    public ArrayList<MedicalTest> getTests(){
        return tests;
    }

    public MedicalTest getTest(int id){
        return tests.get(id);
    }

    public MedicalTest getTest(String name){
        for(MedicalTest test:tests){
            if(test.getName().equals(name))
                return test;
        }
        return null;
    }
    public MedicalTest getTestByUUID(String uuid){
        for(MedicalTest test:tests){
            if(test.getUUID().equals(uuid))
                return test;
        }
        return null;
    }

    public HashMap<String,ArrayList<MedicalTest>> getArrayTestType(){
        HashMap<String,ArrayList<MedicalTest>> arraySortedTestOnType = new HashMap<String,ArrayList<MedicalTest>>();
        for(String type: group){
            ArrayList<MedicalTest> listTest = new ArrayList<MedicalTest>();
            for(MedicalTest test : tests){
                if(type.equals(test.getType())){
                    listTest.add(test);
                }
            }
            arraySortedTestOnType.put(type,listTest);
        }
        return arraySortedTestOnType;
    }

    private ArrayList<String> getAllTypes(){
        return group;
    }




    public boolean getServerTests(){
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet();
        Long modifyDate = manager.getNewerDate(HEADER_ANALYZES, TAG_UPDATED_AT);
        String modify = ""+modifyDate;
        httpGet.setHeader(HEADER_NAME_MODIFI, modify);
        httpGet.setURI(URI.create(SERVER_URL));
        String cookie = sharedPreferences.getString(APP_PREFERENCES_COOKIE, "");
        httpGet.setHeader(APP_PREFERENCES_COOKIE,cookie);
        httpGet.setHeader(HEADER_NAME_X_COMAND, HEADER_ANALYZES);
        try{
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");

            HttpResponse response = defaultHttpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 200){
                String line = EntityUtils.toString(response.getEntity());
                parseFromJson(line);
                initAnalyzes();
                return true;
            }
            if(statusCode == 304){
                return false;
            }
        }catch (IOException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
        return false;
    }
    private void parseFromJson(String response){
        try {
            JSONArray jsonArray = new JSONArray(response);
            for(int i = 0; i < jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                parseToBD(jsonObject);
            }
        }catch (JSONException ex){
            Log.e("JSON analize",ex.getLocalizedMessage());
        }
    }
    private void parseToBD(JSONObject jsonObject){

        SQLiteDatabase writableDatabase = manager.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        try {
            String uuid = jsonObject.getString(TAG_UUID);
            contentValues.put(TAG_UUID, uuid);
            contentValues.put(TAG_NAME, jsonObject.getString(TAG_NAME));
            contentValues.put(TAG_PRICE, jsonObject.getDouble(TAG_PRICE));
            contentValues.put(TAG_DURATION, jsonObject.getInt(TAG_DURATION));
            contentValues.put(TAG_NUMBER, jsonObject.getString(TAG_NUMBER));
            contentValues.put(TAG_ICON, jsonObject.getString(TAG_ICON));
            //contentValues.put(TAG_FULL_DESC, jsonObject.getString(TAG_FULL_DESC));
            contentValues.put(TAG_BRIEF_DESC, jsonObject.getString(TAG_BRIEF_DESC));
            //contentValues.put(TAG_ADDITIONAL_DESC, jsonObject.getString(TAG_ADDITIONAL_DESC));
            contentValues.put(TAG_CATEGORY, jsonObject.getString(TAG_CATEGORY));
            contentValues.put(TAG_DELETED, jsonObject.getString(TAG_DELETED));
            contentValues.put(TAG_UPDATED_AT, jsonObject.getLong(TAG_UPDATED_AT));
            if(!manager.isEqualResult(HEADER_ANALYZES,TAG_UUID,uuid)){
                long id = writableDatabase.insert(HEADER_ANALYZES, null, contentValues);
            }else{
                long id = writableDatabase.update(HEADER_ANALYZES, contentValues, TAG_UUID + " = ?", new String[]{uuid});
            }
        }catch (JSONException ex){
            Log.e("JSON",ex.getLocalizedMessage());
        }finally {
            writableDatabase.close();
        }
    }

    @Override
    public ArrayList<Object> getSearchResult(String query) {
        ArrayList<Object> result = new ArrayList<Object>();

        for(MedicalTest test : tests){
            if(test.getSearchString().toLowerCase().contains(query)){
                result.add(test);
            }
        }
        return result;
    }
}
