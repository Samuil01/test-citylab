package pro.webformula.dev.CLab;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pro.webformula.dev.CLab.UI.ParentActivity;

/**
 * Created by dev on 24.03.2015.
 */
public class NotificationReceiver extends BroadcastReceiver {

    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        Bundle data = intent.getExtras();
        String text = data.getString("text");
        showNotification(text);

    }

    public void showNotification(String text) {


        Intent i = new Intent(context,ParentActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("ACTION",true);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, i, 0);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle("CityLab");
        int j = 0;
        while (j<text.length()){
            if((j+30)<text.length())
                inboxStyle.addLine(text.substring(j,(j+30)));
            else
                inboxStyle.addLine(text.substring(j,text.length()));
            j+=30;
        }
        Notification noti = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setStyle(inboxStyle)
                .setContentTitle("CityLab")
                //.setContentText(text)
                .setContentIntent(pIntent)
                .setSound(soundUri)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setAutoCancel(true).build();
        manager.notify(0,noti);
    }
}