package pro.webformula.dev.CLab.Controllers;

import android.util.Log;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dev on 02.03.2015.
 */
public class FieldValidator {

    public static enum VALIDATOR_KEY{NOTNULL,ONLYTEXT,ANYTEXT,ONLYNUMERIC,MAIL,PHONE,DATE,PASS}
    private static final Calendar c = Calendar.getInstance();
    public static final SimpleDateFormat format = new SimpleDateFormat("d.M.yyyy");
    public static final String regExpOnlyText = "";

    public static VALIDATOR_KEY validate(String value,VALIDATOR_KEY key){
        VALIDATOR_KEY resultKey = VALIDATOR_KEY.PASS;
        switch (key){
            case NOTNULL:
                if(!notNull(value)){
                    resultKey =  VALIDATOR_KEY.NOTNULL;
                }
                break;
            case ONLYTEXT:
                if(!isOnlyText(value)) {
                    resultKey =  VALIDATOR_KEY.ONLYTEXT;
                }
                break;
            case ANYTEXT:
                if(!isAnyText(value)) {
                    resultKey =  VALIDATOR_KEY.ANYTEXT;
                }
                break;
            case MAIL:
                if(!isValidMail(value)) {
                    resultKey =  VALIDATOR_KEY.MAIL;
                }
                break;
            case PHONE:
                if(!isValidPhone(value)) {
                    resultKey =  VALIDATOR_KEY.PHONE;
                }
                break;
            case DATE:
                if(!isCorrectDate(value)) {
                    resultKey =  VALIDATOR_KEY.DATE;
                }
                break;
            case ONLYNUMERIC:
                if(!isOnlyNumeric(value)) {
                    resultKey =  VALIDATOR_KEY.ONLYNUMERIC;
                }
                break;
        }
        return resultKey;
    }

    public static  HashMap<Integer,VALIDATOR_KEY> checkValueByKey(HashMap<Integer,HashMap<String,VALIDATOR_KEY>> validationField){
        HashMap<Integer,VALIDATOR_KEY> validationResult = new  HashMap<Integer,VALIDATOR_KEY>();
        for(Integer position:validationField.keySet()){
            VALIDATOR_KEY key;
            for(String innerValue:validationField.get(position).keySet()){
                key = validate(innerValue,validationField.get(position).get(innerValue));
                if(!key.equals(VALIDATOR_KEY.PASS)){
                    validationResult.put(position,key);
                }
                break;
            }
        }
        return validationResult;
    }

    public static boolean notNull(String value){
        if(value.equals(""))
            return false;
        return true;
    }

    public static boolean isCorrectDate(String value){
        try{
            Date date = format.parse(value);
            if(date.after(c.getTime())){
                return false;
            }

        }catch (ParseException ex){
            Log.e("DATE ERROR: ",ex.getLocalizedMessage() );
            return false;
        }
        return true;
    }

    public static boolean isOnlyText(String value){
        Pattern p = Pattern.compile("([А-ЯЁ][а-яё]+[\\-\\s]?){1,255}");
        Matcher m = p.matcher(value);
        return m.matches();
    }

    public static boolean isAnyText(String value){
        Pattern p = Pattern.compile("([а-яё]+[\\-\\s]?){1,255}");
        Matcher m = p.matcher(value);
        return m.matches();
    }

    public static boolean isValidMail(String value) {
        Pattern p = Pattern.compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Matcher m = p.matcher(value);
        return m.matches();
    }

    public static boolean isValidPhone(String value){
        if(value.length()<11 ||value.length()>12)
            return false;
        Pattern p = Pattern.compile("^\\+7\\d{10}$");
        Matcher m = p.matcher(value);
        return m.matches();
    }
    public static boolean isOnlyNumeric(String value){

        Pattern p = Pattern.compile("([0-9]+[\\-\\s]?){7,10}$");
        Matcher m = p.matcher(value);
        return m.matches();
    }

    public static long getUnixTime(String value){
        try{
            Date date = format.parse(value);
            return date.getTime();
        }catch (ParseException ex){
            Log.e("DATE ERROR: ",ex.getLocalizedMessage() );
            return -1;
        }
    }
    public static int getGender(String value){
        if(value.equals("Мужчина")){
            return 1;
        }else {
            return 2;
        }
    }
}
