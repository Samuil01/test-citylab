package pro.webformula.dev.CLab.UI;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import pro.webformula.dev.CLab.Controllers.FieldValidator;
import pro.webformula.dev.CLab.Controllers.OrderController;
import pro.webformula.dev.CLab.POJO.Customer;
import pro.webformula.dev.CLab.POJO.MedicalTest;
import pro.webformula.dev.CLab.Controllers.MedicalTestController;
import pro.webformula.dev.CLab.POJO.Order;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 17.02.2015.
 */
public class OrderActivity extends ActionBarActivity {

    private ArrayList<MedicalTest> selectedTest;
    private ListView listViewSelectedTest;
    private TestAdapter adapter;
    private ScrollView scrollView;

    private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy");
    private boolean isMen = true;
    private Order.isHave isPregnancy = Order.isHave.No;

    private ImageView textViewCreateOrder;
    private ImageView textViewBack;
    private EditText editTextName;
    private EditText editTextSurname;
    private EditText editTextPatronymic;
    private EditText editTextBirthday;
    private EditText editTextGender;
    private EditText editTextPhone;
    private TextView textViewPregnancyYes;
    private TextView textViewPregnancyNo;
    private TextView textViewPregnancyMaybe;
    private EditText editTextPregnancyDate;
    private CheckBox checkBoxPauseDate;
    private TextView textViewAddNewTest;
    private TextView textViewTitle;
    private ImageView imageViewIconAdd;

    private ArrayList<EditText> validateEditText;
    private ArrayList<TextView> textListPregnancyChoose;

    private boolean isViewOrder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        String uuid = getIntent().getStringExtra(OrdersFragment.ORDER_EXTRA_UUID);
        prepareView();
        if(uuid == null || uuid.equals("")){
            setListener();
        }else{
            setOrderInformation(uuid);
        }
    }

    public void prepareView(){
        scrollView = (ScrollView)findViewById(R.id.scroll);
        listViewSelectedTest = (ListView)findViewById(R.id.order_patient_tests);
        selectedTest = new ArrayList<MedicalTest>();
        adapter = new TestAdapter(getApplicationContext(),selectedTest);
        textListPregnancyChoose = new ArrayList<TextView>();
        validateEditText = new ArrayList<EditText>();

        textViewBack = (ImageView)findViewById((R.id.order_patient_back));
        textViewCreateOrder = (ImageView)findViewById(R.id.order_patient_create_order);
        editTextName = (EditText)findViewById(R.id.order_patient_name);
        editTextSurname = (EditText)findViewById(R.id.order_patient_surname);
        editTextPatronymic = (EditText)findViewById(R.id.order_patient_patronymic);
        editTextBirthday = (EditText)findViewById(R.id.order_patient_birthday);
        editTextGender = (EditText)findViewById(R.id.order_patient_gender);
        editTextPhone = (EditText)findViewById(R.id.order_patient_phone);
        textViewPregnancyYes = (TextView)findViewById(R.id.pregnancy_yes);
        textViewPregnancyNo = (TextView)findViewById(R.id.pregnancy_no);
        textViewPregnancyMaybe = (TextView)findViewById(R.id.pregnancy_maybe);
        editTextPregnancyDate = (EditText)findViewById(R.id.order_patient_pregnancy_date);
        checkBoxPauseDate = (CheckBox)findViewById(R.id.order_patient_pause_date);
        textViewAddNewTest = (TextView) findViewById(R.id.add_new_test);
        textViewTitle = (TextView)findViewById(R.id.order_title);
        imageViewIconAdd = (ImageView)findViewById(R.id.order_add_test_icon);

        validateEditText.add(editTextName);
        validateEditText.add(editTextSurname);
        validateEditText.add(editTextPatronymic);
        validateEditText.add(editTextBirthday);
        validateEditText.add(editTextGender);
        validateEditText.add(editTextPhone);
        validateEditText.add(editTextPregnancyDate);

        textListPregnancyChoose.add(textViewPregnancyNo);
        textListPregnancyChoose.add(textViewPregnancyYes);
        textListPregnancyChoose.add(textViewPregnancyMaybe);


        getActionBar().hide();
        listViewSelectedTest.setAdapter(adapter);
        listViewSelectedTest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MedicalTest medicalTest = (MedicalTest)adapter.getItem(position);
                String desc = medicalTest.getDesk();
                showWebViewDialog(desc);
            }
        });
    }

    public void setListener(){
        isViewOrder = false;
        editTextBirthday.setFocusable(false);
        editTextBirthday.setOnClickListener(
                new UserClickListener(editTextBirthday, UserClickListener.DIALOG_DATE));
        checkBoxPauseDate.setFocusable(false);

        editTextGender.setFocusable(false);
        editTextGender.setOnClickListener(
                new UserClickListener(editTextGender, UserClickListener.DIALOG_GENDER));

        for(final TextView textView: textListPregnancyChoose){
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setPregnancy(textView);
                }
            });
        }

        textViewCreateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isViewOrder)
                    finish();
                if(validateForm()){
                    if(selectedTest.size()>0){
                        createNewOrder();
                        Toast.makeText(getApplicationContext(),"Заявка создана",Toast.LENGTH_SHORT).show();
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),"Выберите анализы",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Неверно внесены данные",Toast.LENGTH_SHORT).show();
                }
            }
        });

        textViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imageViewIconAdd.setImageResource(R.drawable.add);

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.order_add_test);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MedicalTestList.class);
                startActivityForResult(intent, 1);
            }
        });
        /*textViewAddNewTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MedicalTestList.class);
                startActivityForResult(intent, 1);
            }
        });*/
        for(final EditText editText: validateEditText){
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    editText.setTextColor(getResources().getColor(android.R.color.black));
                }
            });
        }
        textViewAddNewTest.setText(getResources().getString(R.string.add_new_test));
    }
    public void setPregnancy(TextView textViewPregnancy){
        if(isMen){
            for(TextView textView: textListPregnancyChoose){
                textView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                textView.setTextColor(getResources().getColor(android.R.color.darker_gray));
                textView.setClickable(false);
            }
            editTextPregnancyDate.setText("");
            editTextPregnancyDate.setHintTextColor(getResources().getColor(android.R.color.darker_gray));
            editTextPregnancyDate.setClickable(false);
            editTextPregnancyDate.setEnabled(false);
            editTextPregnancyDate.clearFocus();
            checkBoxPauseDate.setChecked(false);
            checkBoxPauseDate.setClickable(false);
        }else{
            for(TextView textView: textListPregnancyChoose){
                if(textView.equals(textViewPregnancy)){
                    if(textView.equals(textViewPregnancyNo)){
                        editTextPregnancyDate.setText("");
                        editTextPregnancyDate.setHintTextColor(getResources().getColor(android.R.color.darker_gray));
                        editTextPregnancyDate.setClickable(false);
                        editTextPregnancyDate.clearFocus();
                        editTextPregnancyDate.setEnabled(false);
                        isPregnancy = Order.isHave.No;
                    }else{
                        editTextPregnancyDate.setClickable(true);
                        editTextPregnancyDate.setFocusable(true);
                        editTextPregnancyDate.setEnabled(true);
                        if(textView.equals(textViewPregnancyYes)){
                            isPregnancy = Order.isHave.Yes;
                        }else{
                            isPregnancy = Order.isHave.Maybe;
                        }
                    }
                    textView.setBackgroundColor(getResources().getColor(android.R.color.black));
                    textView.setTextColor(getResources().getColor(android.R.color.white));
                    textView.setClickable(true);
                }else{
                    textView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    textView.setTextColor(getResources().getColor(android.R.color.black));
                    textView.setClickable(true);
                }
            }
            checkBoxPauseDate.setClickable(true);
        }
    }

    public void setOrderInformation(String uuid){
        Order order = OrderController.getController(getApplicationContext()).getOrderByUUID(uuid);
        if(order == null){
            setListener();
            return;
        }
        isViewOrder = true;
        editTextName.setText(order.getCustomer().getName());
        editTextSurname.setText(order.getCustomer().getSurname());
        editTextPatronymic.setText(order.getCustomer().getPatronymics());
        editTextBirthday.setText(format.format(order.getCustomer().getBirthday()));
        editTextGender.setText((order.getCustomer().isGender())?R.string.men:R.string.women);
        editTextPhone.setText(order.getCustomer().getPhone());
        for(TextView textView:textListPregnancyChoose){
            switch (order.getPregnancy().getChildbearing()){
                case No:
                    if(textView.equals(textViewPregnancyNo)){
                        textView.setBackgroundColor(getResources().getColor(android.R.color.black));
                        textView.setTextColor(getResources().getColor(android.R.color.white));
                    }else{
                        textView.setTextColor(getResources().getColor(android.R.color.darker_gray));
                    }
                    break;
                case Yes:
                    if(textView.equals(textViewPregnancyYes)){
                        textView.setBackgroundColor(getResources().getColor(android.R.color.black));
                        textView.setTextColor(getResources().getColor(android.R.color.white));
                    }else{
                        textView.setTextColor(getResources().getColor(android.R.color.darker_gray));
                    }
                    break;
                case Maybe:
                    if(textView.equals(textViewPregnancyMaybe)){
                        textView.setBackgroundColor(getResources().getColor(android.R.color.black));
                        textView.setTextColor(getResources().getColor(android.R.color.white));
                    }else{
                        textView.setTextColor(getResources().getColor(android.R.color.darker_gray));
                    }
                    break;
            }
            textView.setFocusable(false);
            textView.setClickable(false);
        }
        if(order.getPregnancy().getChildbearing() != Order.isHave.No){
            editTextPregnancyDate.setText(""+order.getPregnancy().getPossibleDate());
            editTextPregnancyDate.setTextColor(getResources().getColor(android.R.color.black));
        }
        checkBoxPauseDate.setChecked(order.isPause());
        checkBoxPauseDate.setClickable(false);
        checkBoxPauseDate.setFocusable(false);

        adapter.addAll(order.getTestsList());
        adapter.notifyDataSetChanged();
        setListViewHeightBasedOnChildrenSpec(listViewSelectedTest);

        for(EditText editText:validateEditText){
            editText.setFocusable(false);
            editText.setClickable(false);
        }
        textViewTitle.setText("Заявка");

        textViewCreateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            for(EditText editText:validateEditText){
                editText.setFocusable(true);
                editText.setClickable(true);
            }
                clearTestList();
                setListener();
            }
        });

        textViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        textViewAddNewTest.setText("");
    }

    public boolean validateForm(){
        boolean isValid = true;
        int position = 0;
        for(EditText editText: validateEditText){
            if(editText.getText().toString().equals("")){
                if((isMen||isPregnancy == Order.isHave.No) && (editText.equals(editTextPregnancyDate))) continue;
                editText.setHintTextColor(getResources().getColor(android.R.color.holo_red_light));
                isValid = false;
            }else{
                switch (position){
                    case 6:
                        break;
                    case 3:
                        if(!FieldValidator.isCorrectDate(editText.getText().toString())){
                            editText.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                            isValid = false;
                        }
                        break;
                    case 4:
                        break;
                    case 5:
                        if(!FieldValidator.isValidPhone(editText.getText().toString())){
                            editText.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                            isValid = false;
                        }
                        break;

                    default:
                        if(!FieldValidator.isOnlyText(editText.getText().toString())){
                            editText.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                            isValid = false;
                        }
                        break;
                }
            }
            position++;
        }
        return isValid;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        int[] ids = data.getIntArrayExtra("id");
        addNewTest(ids);
        setListViewHeightBasedOnChildren(listViewSelectedTest);
        scrollView.pageScroll(View.FOCUS_DOWN);
    }

    private void addNewTest(int[] ids){
        for (int i : ids){
            selectedTest.add(MedicalTestController.getController(getApplicationContext()).getTest(i));
        }
        adapter.notifyDataSetChanged();
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
    public void setListViewHeightBasedOnChildrenSpec(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight()/(9-i);
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private class  TestAdapter extends ArrayAdapter<MedicalTest> {
        ArrayList<MedicalTest> values;
        Context context;

        public TestAdapter(Context context, ArrayList<MedicalTest> values){
            super(context, R.layout.item_news, values);
            this.values = values;
            this.context = context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_medical_test, parent, false);
            }

            MedicalTest medicalTest = getItem(position);

            ImageView imageViewIcon = (ImageView)convertView.findViewById(R.id.item_test_img);
            TextView textViewTitle = (TextView)convertView.findViewById(R.id.item_test_title);
            TextView textViewDay = (TextView)convertView.findViewById(R.id.item_test_day);
            TextView textViewCost = (TextView)convertView.findViewById(R.id.item_test_cost);
            TextView textViewId = (TextView)convertView.findViewById(R.id.item_test_id);
            ImageView imageViewSelect = (ImageView)convertView.findViewById(R.id.item_test_selected);

            textViewTitle.setText(medicalTest.getName());
            textViewDay.setText(medicalTest.getTime(1));
            textViewCost.setText(medicalTest.getPrice(true));
            textViewId.setText(medicalTest.getNumber());

            int idIcon = getResources().getIdentifier(medicalTest.getImg(),
                    "drawable", context.getPackageName());
            imageViewIcon.setImageResource(idIcon);

            if(!isViewOrder){
                imageViewSelect.setImageResource(R.drawable.delete);
                imageViewSelect.setOnClickListener(new View.OnClickListener() {
                    int id = position;

                    @Override
                    public void onClick(View v) {
                        selectedTest.remove(getItem(id));
                        adapter.notifyDataSetChanged();
                        setListViewHeightBasedOnChildren(listViewSelectedTest);
                        scrollView.pageScroll(View.FOCUS_DOWN);
                    }
                });
            }
            return convertView;
        }

    }

    public void clearTestList(){
        selectedTest.clear();
        adapter.notifyDataSetChanged();
        setListViewHeightBasedOnChildren(listViewSelectedTest);
        scrollView.pageScroll(View.FOCUS_DOWN);
    }

    public void createNewOrder(){
        Date dateB = new Date();
        int dateP = 0;
        try{
            dateB =format.parse(editTextBirthday.getText().toString());
            if(!editTextPregnancyDate.getText().toString().equals("")){
                dateP = Integer.parseInt(editTextPregnancyDate.getText().toString());
            }
        }catch (ParseException ex){
            Log.e("Parce",ex.getLocalizedMessage());
        }
        Customer customer = new Customer();
        customer.setName(editTextName.getText().toString());
        customer.setSurname(editTextSurname.getText().toString());
        customer.setPatronymics(editTextPatronymic.getText().toString());
        customer.setGender(isMen);
        customer.setPhone(editTextPhone.getText().toString());
        customer.setBirthday(dateB);

        Order order = new Order();
        order.setOrderUUID(UUID.randomUUID().toString());
        order.setCustomer(customer);
        order.setDateCreate(new Date());
        if(isMen||isPregnancy == Order.isHave.No){
            order.setPregnancy(Order.isHave.No,dateP);
        }else{
            order.setPregnancy(isPregnancy,dateP);
        }
        order.setStatus(0);
        order.setPause(checkBoxPauseDate.isChecked());
        order.setTestsList(selectedTest);
        OrderController.getController(getApplicationContext()).insertInBD(order);
    }



    public class UserClickListener implements View.OnClickListener{
        public static final int DIALOG_DATE = 0;
        public static final int DIALOG_GENDER = 1;
        private EditText editText;
        private int dialog;

        UserClickListener(EditText editText, int dialog){
            this.dialog = dialog;
            this.editText = editText;
        }

        @Override
        public void onClick(View v) {
            switch (dialog){
                case DIALOG_DATE:
                    showDatePickerDialog(editText);
                    break;
                case DIALOG_GENDER:
                    showGenderPickerDialog(editText);
                    break;
            }
        }
    }


    public void showDatePickerDialog(EditText editText){
        DatePickerFragment dataPicker = new DatePickerFragment();
        dataPicker.setEditTextElement(editText);
        dataPicker.show(getSupportFragmentManager(), "datePicker");
    }

    public void showGenderPickerDialog(final EditText editText){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.gender)
                .setItems(R.array.genders, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0){
                            editText.setText(R.string.men);
                            isMen = true;
                            setPregnancy(textViewPregnancyNo);
                        }else{
                            editText.setText(R.string.women);
                            isMen = false;
                            isPregnancy = Order.isHave.Yes;
                            setPregnancy(textViewPregnancyYes);
                        }
                    }
                });
        builder.show();
    }

    public void showWebViewDialog(String desc){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Анализ");

        if("".equals(desc) || desc == null){
            alert.setMessage("Раздел находится в наполнении");
        }else{
            WebView wv = new WebView(this);
//          wv.loadUrl(desc);
            wv.loadData(desc, "text/html; charset=UTF-8", null);
            wv.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });

            alert.setView(wv);
        }
        alert.setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alert.show();
    }
}
