package pro.webformula.dev.CLab.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;

import pro.webformula.dev.CLab.POJO.News;
import pro.webformula.dev.CLab.SqlBaseManager;

/**
 * Created by dev on 12.02.2015.
 */
public class NewsController {

    private static final String APP_PREFERENCES = "CityLab";
    public static final String APP_PREFERENCES_COOKIE = "Cookie";

    public static final String SERVER_URL ="http://195.128.126.168:84/api/core.ashx";
    public static final String HEADER_NAME_MODIFI = "If-Modified-Since";
    public static final String HEADER_NAME_X_COMAND = "X-Command";
    public static final String HEADER_NEWS = "News";
    public static final String EXTRA_NEWS = "news";

    public static final String TAG_UUID = "uuid";
    public static final String TAG_CREATED_AT = "created_at";
    public static final String TAG_TITLE = "title";
    public static final String TAG_TEXT = "text";
    public static final String TAG_IMG_URL = "img_url";
    public static final String TAG_DELETED = "deleted";
    public static final String TAG_UPDATED_AT = "updated_at";
    public static final String TAG_IS_ACTION = "is_action";

    private ArrayList<News> newsList;

    private static NewsController controller;
    private static Context context;

    private SqlBaseManager manager;

    private SharedPreferences sharedPreferences;

    private boolean isDownload = false;
    private boolean isDBEmpty = false;
    private boolean isHaveOldNews = true;


    private long lastShowedNewsDate;

    private NewsController(){

        newsList = new ArrayList<News>();
        manager = new SqlBaseManager(context);
        sharedPreferences = context.getApplicationContext()
                .getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        //newsInit();
    }

    public static NewsController getController(Context currContext){
        if(controller == null){
            context = currContext;
            controller = new NewsController();
        }
        return controller;
    }

    public ArrayList<News> getNewsList(){
        SQLiteDatabase base = manager.getReadableDatabase();
        Cursor cursor = base.query(HEADER_NEWS,null,null,null,null,null,TAG_CREATED_AT+" DESC","10");
        if(cursor.moveToFirst()){
            newsList.clear();
            int uuid = cursor.getColumnIndex(TAG_UUID);
            int title = cursor.getColumnIndex(TAG_TITLE);
            int date = cursor.getColumnIndex(TAG_CREATED_AT);
            int img = cursor.getColumnIndex(TAG_IMG_URL);
            int text = cursor.getColumnIndex(TAG_TEXT);
            int isAction = cursor.getColumnIndex(TAG_IS_ACTION);
            do{
                String action = cursor.getString(isAction);
                News news = new News();
                news.setUUID(cursor.getString(uuid));
                news.setTitle(cursor.getString(title));
                news.setDate((new Date(cursor.getLong(date) * 1000)));
                news.setImg(cursor.getString(img));
                news.setDescription(cursor.getString(text));
                news.setAction(action.equals("true")?true:false);
                newsList.add(news);
                lastShowedNewsDate = cursor.getLong(date);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return newsList;
    }

    public void loadNews(){
        if(!isDownload&&isHaveOldNews){
            isDownload = true;
            new NewsTask().execute(false);
        }
    }

    public ArrayList<News> getOlderNews(){

        ArrayList<News> olderNews = new ArrayList<News>();
        SQLiteDatabase base = manager.getReadableDatabase();
        String selection = TAG_CREATED_AT+" < ?";
        String[] selectionArgs = new String[] { ""+lastShowedNewsDate };
        Cursor cursor = base.query(HEADER_NEWS,null,selection,selectionArgs,null,null,TAG_CREATED_AT+" DESC","4");
        if(cursor.moveToFirst()){
            int uuid = cursor.getColumnIndex(TAG_UUID);
            int title = cursor.getColumnIndex(TAG_TITLE);
            int date = cursor.getColumnIndex(TAG_CREATED_AT);
            int img = cursor.getColumnIndex(TAG_IMG_URL);
            int text = cursor.getColumnIndex(TAG_TEXT);
            int isAction = cursor.getColumnIndex(TAG_IS_ACTION);
            do{
                String action = cursor.getString(isAction);
                News news = new News();
                news.setUUID(cursor.getString(uuid));
                news.setTitle(cursor.getString(title));
                news.setDate((new Date(cursor.getLong(date) * 1000)));
                news.setImg(cursor.getString(img));
                news.setDescription(cursor.getString(text));
                news.setAction(action.equals("true")?true:false);
                olderNews.add(news);
                lastShowedNewsDate = cursor.getLong(date);
            }while (cursor.moveToNext());
        }else{
            isDBEmpty = true;
        }
        cursor.close();
        return olderNews;
    }

    public void newsInit(){
        new NewsTask().execute(true);
    }

    public void setNewsList(){
        SQLiteDatabase base = manager.getReadableDatabase();
        Cursor cursor = base.query(HEADER_NEWS,null,null,null,null,null,TAG_CREATED_AT);
        if(cursor.moveToFirst()){
            int title = cursor.getColumnIndex(TAG_TITLE);
            int date = cursor.getColumnIndex(TAG_CREATED_AT);
            int img = cursor.getColumnIndex(TAG_IMG_URL);
            int text = cursor.getColumnIndex(TAG_TEXT);
            int isAction = cursor.getColumnIndex(TAG_IS_ACTION);
            do{
                String action = cursor.getString(isAction);
                News news = new News();
                news.setTitle(cursor.getString(title));
                news.setDate((new Date(cursor.getLong(date)*1000)));
                news.setImg(cursor.getString(img));
                news.setDescription(cursor.getString(text));
                news.setAction(action.equals("true")?true:false);
                newsList.add(news);
            }while (cursor.moveToNext());
        }
    }

    public boolean isDownload(){
        return isDownload;
    }

    public boolean isAvailableOldNews(){
        return isHaveOldNews;
    }

    public void newsReset(){
        isHaveOldNews = true;
        isDBEmpty=false;
    }
    public boolean isDBEmpty(){
        return isDBEmpty;
    }


    public boolean isUpdatedNews(){
        return false;
    }

    private class NewsTask extends AsyncTask<Boolean,Void,Void> {
        @Override
        protected Void doInBackground(Boolean... params) {
            isDownload = true;
            getServerNews(params[0]);
            return null;
        }
    }

    public boolean getServerNews(boolean isUpdate){
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet();
        if(isUpdate){
            Long modifyDate = new SqlBaseManager(context).getNewerDate(HEADER_NEWS, TAG_UPDATED_AT);
            String modify = ""+modifyDate;
            httpGet.setHeader(HEADER_NAME_MODIFI, modify);
            String uri = SERVER_URL+"?count=15";
            httpGet.setURI(URI.create(uri));
        }else{
            Long olderDate = new SqlBaseManager(context).getOlderDate(HEADER_NEWS, TAG_CREATED_AT);
            String uri = SERVER_URL+"?before_date="+olderDate+"&count=7";
            httpGet.setURI(URI.create(uri));
        }
        String cookie = sharedPreferences.getString(APP_PREFERENCES_COOKIE, "");
        httpGet.setHeader(APP_PREFERENCES_COOKIE,cookie);
        httpGet.setHeader(HEADER_NAME_X_COMAND, HEADER_NEWS);
        try{
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");

            HttpResponse response = defaultHttpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 200){
                if(isUpdate){
                    new SqlBaseManager(context).clearTable(HEADER_NEWS);
                    isHaveOldNews = true;
                    isDBEmpty = false;
                }
                String line = EntityUtils.toString(response.getEntity());
                parseFromJson(line);
                return true;
            }
            if(statusCode == 304){
                if(!isUpdate)
                    isHaveOldNews = false;
                isDownload = false;
                return false;
            }
        }catch (IOException ex){
            Log.e("IO",ex.getLocalizedMessage());
        }
        isDownload = false;
        return false;
    }
    private void parseFromJson(String response){
        try {
            JSONArray jsonArray = new JSONArray(response);
            for(int i = 0; i < jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                parseToBD(jsonObject);
            }
        }catch (Exception ex){
            Log.e("JSON",ex.getLocalizedMessage());
        }
    }
    private void parseToBD(JSONObject jsonObject){

        SQLiteDatabase writableDatabase = manager.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        try {
            String uuid = jsonObject.getString(TAG_UUID);
            contentValues.put(TAG_UUID, uuid);
            contentValues.put(TAG_CREATED_AT, jsonObject.getLong(TAG_CREATED_AT));
            contentValues.put(TAG_TITLE, jsonObject.getString(TAG_TITLE));
            contentValues.put(TAG_TEXT, jsonObject.getString(TAG_TEXT));
            contentValues.put(TAG_IMG_URL, jsonObject.getString(TAG_IMG_URL));
            contentValues.put(TAG_DELETED, jsonObject.getString(TAG_DELETED));
            contentValues.put(TAG_UPDATED_AT, jsonObject.getLong(TAG_UPDATED_AT));
            contentValues.put(TAG_IS_ACTION, jsonObject.getBoolean(TAG_IS_ACTION)?"true":"false");
            if(!manager.isEqualResult(HEADER_NEWS,TAG_UUID,uuid)){
                long id = writableDatabase.insert(HEADER_NEWS, null, contentValues);
            }
        }catch (JSONException ex){
            Log.e("JSON",ex.getLocalizedMessage());
        }
    }
}
