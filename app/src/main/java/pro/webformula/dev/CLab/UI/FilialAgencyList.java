package pro.webformula.dev.CLab.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import pro.webformula.dev.CLab.Controllers.FilialAgencyController;
import pro.webformula.dev.CLab.POJO.FilialAgency;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 20.03.2015.
 */
public class FilialAgencyList extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState)
    {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filial_agency_list);

        ExpandableListView listView = (ExpandableListView)findViewById(R.id.test_expandable_list);

        ImageView imageView = (ImageView)findViewById(R.id.item_filial_back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        HashMap<String,ArrayList<FilialAgency>> groups = FilialAgencyController.getController(getApplicationContext()).getArrayFilialRegion();
        final ExpListAdapter adapter = new ExpListAdapter(getApplicationContext(), groups);
        listView.setAdapter(adapter);
        for(int i =0;i<adapter.getGroupCount();i++){
            listView.expandGroup(i);
        }
        getActionBar().hide();
    }

    class ExpListAdapter extends BaseExpandableListAdapter {

        private HashMap<String,ArrayList<FilialAgency>> test;
        private ArrayList<String> handlerGroup;
        private Context context;

        public ExpListAdapter (Context context,HashMap<String,ArrayList<FilialAgency>> test){
            this.context = context;
            this.test = test;
            handlerGroup = new ArrayList<String>();
            for(String type: test.keySet()){
                handlerGroup.add(type);
            }
        }

        @Override
        public int getGroupCount() {
            return test.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return test.get(((String)getGroup(groupPosition))).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return handlerGroup.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return test.get((String)getGroup(groupPosition)).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                                 ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.expandbale_list_item, null);
            }

            TextView textGroup = (TextView) convertView.findViewById(R.id.expand_test_type);
            textGroup.setText((String)getGroup(groupPosition));

            return convertView;

        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild,
                                 View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_filial, parent, false);
            }

            final FilialAgency filialAgency = (FilialAgency)getChild(groupPosition,childPosition);

            TextView textViewTitle = (TextView)convertView.findViewById(R.id.filial_name);
            TextView textViewTime = (TextView)convertView.findViewById(R.id.filial_adress);
            textViewTitle.setText(filialAgency.getAddress());
            textViewTime.setText(filialAgency.getPhone());
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int idFilial=0;
                    for(FilialAgency filial: FilialAgencyController.getController(getApplicationContext()).getFilialAgencies()){
                        if(filial.equals(filialAgency)){
                            Intent intent = new Intent(getApplicationContext(),FilialAgencyActivity.class);
                            intent.putExtra(MapsFragment.EXTRA_FILIAL,idFilial);
                            startActivityForResult(intent,1);
                            break;
                        }
                        idFilial++;
                    }
                }
            });
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }

}
