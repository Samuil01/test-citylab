package pro.webformula.dev.CLab.POJO;

import android.location.Address;

import java.util.HashMap;

/**
 * Created by dev on 16.02.2015.
 */
public class FilialAgency {

    private String uuid;
    private String phone;
    private String site;


    private String address;
    private String location;
    private String region;

    private double coordinateX;
    private double coordinateY;

    private Address mapAddress;

    private String time;


    public String getUUID() {
        return uuid;
    }

    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMapAddress(Address mapAddress){
        this.mapAddress = mapAddress;
    }

    public Address getMapAddress(){
        return mapAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public double getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(double coordinateX) {
        this.coordinateX = coordinateX;
    }

    public double getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(double coordinateY) {
        this.coordinateY = coordinateY;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
