package pro.webformula.dev.CLab.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pro.webformula.dev.CLab.Controllers.UserProcessedController;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 17.03.2015.
 */
public class ResultActivity extends ActionBarActivity {
    private UserProcessedController controller;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result);
        controller = UserProcessedController.getController(getApplicationContext());


        getActionBar().setIcon(R.drawable.trp);

        int status = getIntent().getIntExtra("status",1);

        TextView textViewTitle = (TextView)findViewById(R.id.verify_title);
        TextView textViewStatus = (TextView)findViewById(R.id.verify_status);

        String[] arrayTitle = getResources().getStringArray(R.array.status_title);
        String[] arrayStatus = getResources().getStringArray(R.array.status_text);

        textViewTitle.setText(arrayTitle[status]);
        textViewStatus.setText(arrayStatus[status]);
    }
}
