package pro.webformula.dev.CLab.UI;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import pro.webformula.dev.CLab.Controllers.UserProcessedController;
import pro.webformula.dev.CLab.R;

/**
 * Created by dev on 11.02.2015.
 */
public class SettingsFragment extends Fragment {

    private static final String APP_PREFERENCES = "CityLab";
    public static final String APP_PREFERENCES_COOKIE = "Cookie";
    public static final String PROPERTY_REG_ID = "registration_id";

    public static final String SERVER_URL = "http://195.128.126.168:84/api/core.ashx";
    public static final String HEADER_NAME_X_COMAND = "X-Command";
    public static final String HEADER_TYPE_ACTIONS = "Actions";
    public static final String HEADER_TYPE_HOLIDAY = "Holidays";
    public static final String HEADER_TYPE_EXIT = "Exit";


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private Switch switchActions;
    private Switch switchHolidays;

    private String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setings, null);

        sharedPreferences = getActivity().getApplicationContext()
                .getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        token = sharedPreferences.getString(PROPERTY_REG_ID, "");

        LinearLayout linearLayoutAbout = (LinearLayout) view.findViewById(R.id.about_layout);
        linearLayoutAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), AboutActivity.class);
                getActivity().startActivity(intent);
            }
        });

        LinearLayout linearLayoutQuality = (LinearLayout) view.findViewById(R.id.quality_layout);
        linearLayoutQuality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Quality application", Toast.LENGTH_SHORT).show();
            }
        });

        LinearLayout linearLayoutCallback = (LinearLayout) view.findViewById(R.id.callback_callback);
        linearLayoutCallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), FeedBackActivity.class);
                getActivity().startActivity(intent);
            }
        });

        TextView textViewLogout = (TextView) view.findViewById(R.id.tx_user_logout);
        textViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SettingSender().execute(3);
                UserProcessedController.getController(getActivity().getApplicationContext()).setLogin(false);
                Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });

        TextView mailEditText = (TextView) view.findViewById(R.id.user_mail);
        mailEditText.setText(sharedPreferences.getString(UserProcessedController.APP_PREFERENCES_USER_MAIL, ""));

        boolean action = sharedPreferences.getBoolean(HEADER_TYPE_ACTIONS, false);
        switchActions = (Switch) view.findViewById(R.id.actions_switch);
        switchActions.setChecked(action);
        switchActions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchActions.isChecked()) {
                    editor.putBoolean(HEADER_TYPE_ACTIONS, true);
                    new SettingSender().execute(2);
                } else {
                    editor.putBoolean(HEADER_TYPE_ACTIONS, false);
                    new SettingSender().execute(2);
                }
                editor.apply();
            }
        });
        boolean holiday = sharedPreferences.getBoolean(HEADER_TYPE_HOLIDAY, false);
        switchHolidays = (Switch) view.findViewById(R.id.holidays);
        switchHolidays.setChecked(holiday);
        switchHolidays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchHolidays.isChecked()) {
                    editor.putBoolean(HEADER_TYPE_HOLIDAY, true);
                    new SettingSender().execute(1);
                } else {
                    editor.putBoolean(HEADER_TYPE_HOLIDAY, false);
                    new SettingSender().execute(1);
                }
                editor.apply();
            }
        });
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public class SettingSender extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            sendSettings(params[0]);
            return null;
        }
    }


    public void sendSettings(int type) {

        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(SERVER_URL);
        String cookie = sharedPreferences.getString(APP_PREFERENCES_COOKIE, "");
        httpPost.setHeader(APP_PREFERENCES_COOKIE, cookie);
        JSONObject object = new JSONObject();
        try {
            switch (type) {
                case 1:
                    httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_HOLIDAY);
                    object.put("holidays", switchHolidays.isChecked());
                    break;
                case 2:
                    httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_ACTIONS);
                    object.put("actions", switchActions.isChecked());
                    break;
                case 3:
                    httpPost.setHeader(HEADER_NAME_X_COMAND, HEADER_TYPE_EXIT);
                    object.put("token", token);
                    break;
            }
            StringEntity se = new StringEntity(object.toString(), "UTF-8");

            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse response = defaultHttpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                Log.e("RES", "Send settings");
            }
            if (statusCode == 400) {
                String responseBody = EntityUtils.toString(response.getEntity());
                JSONObject objectRes = new JSONObject(responseBody);
                int codeRes = objectRes.getInt("error_code");
                Log.e("CODE", "" + codeRes);
            }
            Log.e("StatusCODE", "" + statusCode);
        } catch (UnsupportedEncodingException ex) {
            Log.e("ENCODING", ex.getLocalizedMessage());
        } catch (IOException ex) {
            Log.e("IO", ex.getLocalizedMessage());
        } catch (JSONException ex) {
            Log.e("IO", ex.getLocalizedMessage());
        }
    }
}